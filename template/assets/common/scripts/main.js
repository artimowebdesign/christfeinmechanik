import $ from 'jquery';
// Set jQuery global
window.$ = window.jQuery = $;

import 'popper.js';
import 'bootstrap';
import 'fancybox';
import 'slick-carousel';
import 'jquery.mmenu';