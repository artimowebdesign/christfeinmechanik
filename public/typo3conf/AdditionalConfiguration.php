<?php
$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->loadEnv(__DIR__ . '/../../.env');

// Database Credentials
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['charset'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_CHARSET') ?? 'utf8';
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['driver'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_DRIVER') ?? 'pdo_mysql';
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_HOST');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_PORT') ?? 3306;
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_USER');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_NAME');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('TYPO3_DB_CONNECTIONS_DEFAULT_PASS');

// Graphics
$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor'] = getenv('TYPO3_GFX_PROCESSOR');
$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = getenv('TYPO3_GFX_PROCESSOR_PATH');
$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = getenv('TYPO3_GFX_PROCESSOR_PATH_LZW');

// System
$GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = getenv('TYPO3_TRUSTED_HOST_PATTERN');

// DDEV
if (getenv('IS_DDEV_PROJECT') === 'true') {
    // Graphics
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor'] = 'ImageMagick';
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = '/usr/bin/';
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = '/usr/bin/';

    // Mail
    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'smtp';
    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = 'localhost:1025';

    // System
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;
}

// Cleanup symfony dotenv vars for security reasons
foreach (explode(',', $_ENV['SYMFONY_DOTENV_VARS']) as $envVar) {
    unset($_ENV[$envVar], $_SERVER[$envVar]);
    putenv($envVar);
}
unset($_ENV['SYMFONY_DOTENV_VARS'], $_SERVER['SYMFONY_DOTENV_VARS']);

// Extension: backend
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend'] = [
    'loginBackgroundImage' => 'EXT:dia_site_common/Resources/Public/Images/Backend/background.svg',
    'loginFootnote' => '© 2019 CHRIST-Feinmechanik GmbH & Co. KG',
    'loginHighlightColor' => '#1c2b52',
    'loginLogo' => 'EXT:dia_site_common/Resources/Public/Images/Backend/logo.svg'
];

// Extension: bootstrap_package
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['bootstrap_package'] = [
    'disableCssProcessing' => '1',
    'disableFontLoader' => '0',
    'disableGoogleFontCaching' => '0',
    'disablePageTsBackendLayouts' => '1',
    'disablePageTsContentElements' => '1',
    'disablePageTsRTE' => '0',
    'disablePageTsTCEFORM' => '0',
    'disablePageTsTCEMAIN' => '0',
];
