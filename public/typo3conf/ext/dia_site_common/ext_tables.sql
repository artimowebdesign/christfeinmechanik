#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	gutter_width tinytext,
    image_width tinytext,
    frame_container_disable tinyint(3) unsigned DEFAULT '0' NOT NULL,
    frame_inner_background_color_class varchar(255) DEFAULT '' NOT NULL,
    image_scale_y tinyint(3) unsigned DEFAULT '0' NOT NULL
);
