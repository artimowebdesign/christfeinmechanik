<?php
defined('TYPO3_MODE') or die();

call_user_func(function($_EXTKEY, $_TABLE) {
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';

    $TCAcolumns = [];

    /**
     * Gutter width
     */
    $TCAcolumns['gutter_width'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.gutter_width.label',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => 0,
            'items' => [
                [$languageLocalFilePrefix . 'tt_content.gutter_width.0' => 0]
            ]
        ]
    ];

    /**
     * Image width and scale
     */
    $imageWidthItems = [
        [$languageLocalFilePrefix . 'tt_content.image_width.0' => 0]
    ];
    for($i=0; $i<=12; $i++) {
        $imageWidthItems[] = [$languageLocalFilePrefix . 'tt_content.image_width.' . $i, $i];
    }
    $TCAcolumns['image_width'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.image_width.label',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => 0,
            'items' => $imageWidthItems
        ]
    ];
    $TCAcolumns['image_scale_y'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.image_scale_y.label',
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                    'labelUnchecked' => $languageLocalFilePrefix . 'tt_content.image_scale_y.0',
                    'labelChecked' => $languageLocalFilePrefix . 'tt_content.image_scale_y.1',
                ]
            ],
        ]
    ];

    /**
     * Frame Container - Disable
     */
    $TCAcolumns['frame_container_disable'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.frame_container_disable.label',
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                    'labelUnchecked' => $languageLocalFilePrefix . 'tt_content.frame_container_disable.0',
                    'labelChecked' => $languageLocalFilePrefix . 'tt_content.frame_container_disable.1',
                ]
            ],
        ]
    ];

    /**
     * Frame Inner - Background Color
     */
    $TCAcolumns['frame_inner_background_color_class'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.frame_inner_background_color_class.label',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['none', 'none']
            ]
        ]
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'frames',
        '--linebreak--, frame_container_disable;' . $languageLocalFilePrefix . 'tt_content.frame_container_disable.label, --linebreak--, frame_inner_background_color_class;' . $languageLocalFilePrefix . 'tt_content.frame_inner_background_color_class.label,',
        'after:space_inner_after_class'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'gallerySettings',
        'gutter_width;' . $languageLocalFilePrefix . 'tt_content.gutter_width.label, image_width;' . $languageLocalFilePrefix . 'tt_content.image_width.label, image_scale_y;' . $languageLocalFilePrefix . 'tt_content.image_scale_y.label,',
        'after:imagecols'
    );

}, 'dia_site_common', 'tt_content');
