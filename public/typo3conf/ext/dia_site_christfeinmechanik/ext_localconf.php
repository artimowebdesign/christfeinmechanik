<?php
defined("TYPO3_MODE") or die();

// Add default RTE configuration
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['dia_site_christfeinmechanik'] = 'EXT:dia_site_christfeinmechanik/Configuration/RTE/Default.yaml';