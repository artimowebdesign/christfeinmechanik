'use strict';

(function() {

  const CLASS_DEFAULT = 'list-triangle';

  CKEDITOR.plugins.add('diasitechristfeinmechanik_list', {
    init: function(editor) {
      editor.on('removeFormatCleanup', function(event) {
        var element = event.data;

        if (element.is('ul')) {
          element.addClass(CLASS_DEFAULT);
        }
      });
    },
    afterInit: function (editor) {
      var dataProcessor = editor.dataProcessor,
        dataFilter = dataProcessor && dataProcessor.dataFilter,
        htmlFilter = dataProcessor && dataProcessor.htmlFilter;

      if (dataFilter) {
        dataFilter.addRules({
          elements: {
            ul: function(element) {
              var attributes = element.attributes,
                  cssClass = attributes['class'];

              if (!cssClass || cssClass.indexOf(CLASS_DEFAULT) !== -1) {
                attributes[ 'class' ] = CLASS_DEFAULT;
              }
            }
          }
        });
      }

      if (htmlFilter) {
        htmlFilter.addRules({
          elements: {
            ul: function(element) {
              var attributes = element.attributes,
                  cssClass = attributes['class'];

              if (!cssClass || cssClass.indexOf(CLASS_DEFAULT) !== -1) {
                attributes[ 'class' ] = CLASS_DEFAULT;
              }
            }
          }
        });
      }

    }
  });

})();
