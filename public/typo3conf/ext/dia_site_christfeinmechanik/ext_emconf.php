<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA Site Christfeinmechanik',
    'description' => 'DIA Site Christfeinmechanik',
    'category' => 'templates',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.99.99',
            'dia_site' => '9.0.0-9.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'Dia\\DiaSiteChristfeinmechanik\\' => 'Classes',
        ],
    ],
];