<?php
defined("TYPO3_MODE") or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    "dia_site_christfeinmechanik",
    "Configuration/TypoScript",
    "DIA Site Christfeinmechanik"
);