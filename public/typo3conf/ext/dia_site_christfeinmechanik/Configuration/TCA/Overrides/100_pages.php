<?php
defined("TYPO3_MODE") or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    "dia_site_christfeinmechanik",
    "Configuration/TsConfig/Page.tsconfig",
    "DIA Site Christfeinmechanik"
);