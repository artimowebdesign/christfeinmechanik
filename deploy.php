<?php
namespace Deployer;

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

require 'recipe/typo3.php';

if (empty(getenv('DEPLOYER_APPLICATION'))) { die('DEPLOYER_APPLICATION ENV required' . "\n"); }
if (empty(getenv('DEPLOYER_REPOSITORY'))) { die('DEPLOYER_REPOSITORY ENV required' . "\n"); }

// Project name
set('application', getenv('DEPLOYER_APPLICATION'));

// Project repository
set('repository', getenv('DEPLOYER_REPOSITORY'));

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);
set('bin/php', '');

// Shared files/dirs between deploys
set('shared_files', [
    '{{typo3_webroot}}/typo3conf/LocalConfiguration.php',
    'auth.json',
    '.env.local',
]);
add('shared_dirs', [
    'var/session',
    'var/labels',
]);

// Writable dirs by web server
set('writable_dirs', [
    '{{typo3_webroot}}/fileadmin',
    '{{typo3_webroot}}/typo3temp',
    '{{typo3_webroot}}/uploads'
]);

// DocumentRoot
set('typo3_webroot', 'public');

// Composer
set('composer_action', 'install');
set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest');

// Hosts


// Live deployment

host('home139664405.1and1-data.host')
    ->user('u38929136')
    ->stage('production')
    ->set('git_tty', false)
    ->set('bin/php', '/usr/bin/php7.3-cli')
    ->set('writable_mode', 'chmod')
    ->set('http_user', 'u38929136')
    ->set('keep_releases', 2)
    ->set('branch', 'master')
    ->set('deploy_path', '/kunden/homepages/4/d139664405/htdocs/typo3/{{application}}')
    ->set('database_backup_path', '{{deploy_path}}/backup/database/');


// Preview deployment
/*
host('home139664405.1and1-data.host')
    ->user('u38929136')
    ->stage('preview')
    ->set('git_tty', false)
    ->set('bin/php', '/usr/bin/php7.3-cli')
    ->set('writable_mode', 'chmod')
    ->set('http_user', 'u38929136')
    ->set('keep_releases', 2)
    ->set('branch', 'v10-upgrade')
    ->set('deploy_path', '/kunden/homepages/4/d139664405/htdocs/typo3/preview/{{application}}')
    ->set('database_backup_path', '{{deploy_path}}/backup/database/');
*/
// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});
task('create_auth', function () {
    $sharedPath = "{{deploy_path}}/shared";
    if (!test("[ -f $sharedPath/auth.json ]")) {
        run("echo '{}' > $sharedPath/auth.json");
    }
});
before('deploy:shared', 'create_auth');

task('database:export', function () {
    $fileName = date('Y-m-d_H-i-s', time()) . '.sql';

    run('if [ ! -d {{database_backup_path}} ]; then mkdir -p {{database_backup_path}}; fi');
    run('{{bin/php}} {{release_path}}/bin/typo3cms database:export -c Default -e \'cf_*\' -e \'cache_*\' -e \'[bf]e_sessions\' -e sys_log > {{database_backup_path}}' . $fileName);

    writeln('Database dump created: <fg=green>' . $fileName . '</>');
});

task('database:download', function () {
    if (!is_dir('backup'))
        mkdir('backup');

    download('{{database_backup_path}}', './backup/database/');
});

task('shared_dirs:download', function () {
    $sharedDirectories = get('shared_dirs');

    foreach ($sharedDirectories as $directory) {
        $directory = get('deploy_path') . '/shared/' . $directory;
        download($directory, 'backup/shared_dirs/');
        writeln($directory . ' successfully downloaded!');
    };
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('shared_dirs:mv', function () {
    runLocally('rsync -aviuzP backup/shared_dirs/fileadmin {{typo3_webroot}}');
    runLocally('rsync -aviuzP backup/shared_dirs/uploads {{typo3_webroot}}');
    runLocally('rsync -aviuzP backup/shared_dirs/typo3temp {{typo3_webroot}}');
});

task('database:import',function(){
    $newestDump = null;
    $dir = __DIR__ . '/backup/database/';
    $files = scandir($dir);
    $time = 0;
    foreach($files as $file){
        $path = $dir . $file;
        $mtime = filemtime($path);
        if($file !== '.' && $file !== '..' && $mtime > $time){
            $newestDump = $path;
            $time = $mtime;
        }
    }
    runLocally('ddev import-db --src='.$newestDump);
});

task('database:updateschema',function(){
    runLocally('ddev exec bin/typo3cms database:updateschema');
});

task('pull', function(){
    invoke('database:export');
    invoke('database:download');
    invoke('database:import');
    invoke('database:updateschema');
    invoke('shared_dirs:download');
    invoke('shared_dirs:mv');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

