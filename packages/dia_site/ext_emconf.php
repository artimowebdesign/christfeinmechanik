<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA Site',
    'description' => 'DIA Site',
    'category' => 'distribution',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '12.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.14-10.99.99',
            'bootstrap_package' => '12.0.0-12.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
