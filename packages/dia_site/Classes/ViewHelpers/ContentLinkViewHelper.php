<?php
namespace Dia\DiaSite\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Link\TypolinkViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Class ContentLinkViewHelper
 * @package Dia\DiaSite\ViewHelpers
 */
class ContentLinkViewHelper extends TypolinkViewHelper
{
    /**
     * Render
     *
     * @inheritDoc
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $content = (string)$renderChildrenClosure();

        if (!empty($arguments['parameter']) && !preg_match('/<a /', $content)) {
            $content = parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
        }

        return $content;
    }
}
