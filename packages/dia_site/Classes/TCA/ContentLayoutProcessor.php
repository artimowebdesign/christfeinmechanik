<?php
namespace Dia\DiaSite\TCA;

class ContentLayoutProcessor {

    protected static function &getConfig(){
      return $GLOBALS['TCA']['tt_content']['columns']['layout']['config']['itemsProcConfig'];
    }

    public static function register(string $CType, ...$layouts){
      $conf = &self::getConfig();
      if(!is_array($conf['CType'])){
        $conf['CType'] = [];
      }
      if(!is_array($conf['CType'][$CType])){
        $conf['CType'][$CType] = ['items'=>[],'templates'=>[]];
      }
      foreach($layouts as $layout){
        list($label,$layoutId,$templateName) = $layout;
        $conf['CType'][$CType]['items'][] = [$label,$layoutId];
        $conf['CType'][$CType]['templates'][$layoutId] = $templateName;
      }
    }

    public static function getTemplateName(string $CType, int $layoutId){
      $conf = &self::getConfig();
      if(!array_key_exists('CType',$conf) || !array_key_exists($CType,$conf['CType'])) return null;
      return $conf['CType'][$CType]['templates'][$layoutId] ?? null;
    }

    public function getItems(&$config) {
        $CType = $config['row']['CType'][0];
        if($config['config']['itemsProcConfig']['CType'][$CType]){
            $config['items'] = array_merge($config['items'],$config['config']['itemsProcConfig']['CType'][$CType]['items']);
        }
    }
}
