<?php
namespace Dia\DiaSite\Boilerplate;
use TYPO3\CMS\Core\Utility\ArrayUtility;

class PhpFile extends File {

    public function __toString(){
        $prefix = count($this->data) && strpos($this->data[0],'<?php') > -1 ? '' : "<?php \n";
        return $prefix . implode($this->pretty?"\n":"",$this->data);
    }

    public function assign(string $varname, $value):File{
        return $this->append("$varname = ".$this->serialize($value).";");
    }

    public function serialize($value) {
        if(is_object($value)){
            return 'unserialize(\'' . serialize($value) . '\')';
        }else if(is_array($value)){
            return ArrayUtility::arrayExport($value);
        }else if(is_bool($value)){
            return $value ? "'true'" : "'false'";
        }else if(is_numeric($value)){
            return (string)$value;
        }else if(is_null($value)){
            return "'null'";
        }else{
            return "'".(string)$value."'";
        }
    }
}
