<?php
namespace Dia\DiaSite\Boilerplate;

use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;

class SitePackageBoilerplate extends AbstractBoilerplate {

    /**
     * @var SiteConfiguration
     */
    private $siteConfiguration;

    public function getSiteConfiguration():SiteConfiguration {
        if(!$this->siteConfiguration){
            $this->siteConfiguration = GeneralUtility::makeInstance(
                SiteConfiguration::class,
                Environment::getConfigPath() . '/sites'
            );
        }

        return $this->siteConfiguration;
    }

    public function setSiteConfiguration(SiteConfiguration $siteConfiguration):SitePackageBoilerplate {

        if(is_null($siteConfiguration)){
            throw new \Exception('site configuration must not be null');
        }

        $this->siteConfiguration = $siteConfiguration;

        return $this;
    }

    protected function createContext(?array $overrides = []):array {
        $context = parent::createContext(
            array_merge(
                [
                    'company' => 'DIA - die.interaktiven GmbH & Co. KG',
                    'category' => 'templates',
                    'keywords' => 'TYPO3, Site Package, Extension',
                    'type' => 'typo3-cms-extension',
                    'homepage' => 'https://die-interaktiven.de/',
                    'stability' => 'dev',
                    'version' => '1.0.0',
                    'typo3MinVersion' => '10.4.14',
                    'typo3MaxVersion' => '10.99.99',
                    'minimumStability' => 'dev',
                    'rootPageUid' => -1,
                    'errorPageUid' => -1,
                    'storagePageUid' => -1
                ],
                $overrides
            )
        );

        $context['extensionKeyPascal'] = GeneralUtility::underscoredToUpperCamelCase($context['extensionKey']);

        $context['autoloadPsr4'] = 'Dia\\\\\\' . $context['extensionKeyPascal'] . '\\\\\\';

        return $context;
    }

    protected function getExtensionKeyForName(string $extensionName):string {
        return parent::getExtensionKeyForName("site-$extensionName");
    }

    protected function getPackageNameForName(string $extensionName):string {
        return parent::getPackageNameForName("site-$extensionName");
    }

    protected function postMigrate(){
        if($this->get('extensionName') !== 'common'){
            $this->createSiteConfiguration();
        }
    }

    protected function getMigrations():array {
        return [
            function(ConnectionPool $pool){ $this->migratePages($pool); }
        ];
    }

    protected function getFiles():array {
        $context = $this->getContext();
        $isCommon = $context['extensionName'] === 'common';
        $extPath = DIRECTORY_SEPARATOR . trim(realpath(dirname(__FILE__) . '/../../'),DIRECTORY_SEPARATOR);
        $boilerplateResources = $extPath . '/Resources/Private/Boilerplate/SitePackageBoilerplate';
        $commonPath = $boilerplateResources . DIRECTORY_SEPARATOR .'Common';
        $customPath = $boilerplateResources . DIRECTORY_SEPARATOR .'Custom';
        $autoPath = $isCommon ? $commonPath : $customPath;

        $files = [
            $this->file('.gitignore')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_emconf.php')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_localconf.php')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_tables.php')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_tables_static+adt.sql')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_tables.sql')->mirrorFrom($commonPath)->compile($context),
            $this->file('composer.json')->mirrorFrom($commonPath)->compile($context),
            $this->file('ext_icon.svg')->appendFiles($extPath . '/ext_icon.svg'),
            $this->file('.editorconfig')->appendFiles($extPath . '/.editorconfig'),
            $this->file('README.md')->mirrorFrom($commonPath)->compile($context),

            $this->file('Classes/Controller/.gitkeep'),
            $this->file('Classes/Domain/.gitkeep'),
            $this->file('Classes/ViewHelpers/.gitkeep'),
            $this->file('Classes/Service/.gitkeep'),
            $this->file('Classes/Utility/.gitkeep'),

            $this->file('Configuration/RTE/Default.yaml')->mirrorFrom($autoPath)->compile($context),
            $this->file('Configuration/TCA/Overrides/pages.php')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TCA/Overrides/sys_template.php')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TsConfig/Page.tsconfig')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TsConfig/Page/RTE/Default.tsconfig')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TsConfig/Page/Mod/.gitkeep'),
            $this->file('Configuration/TsConfig/Page/TCEFORM/.gitkeep'),
            $this->file('Configuration/TsConfig/Page/TCEMAIN/.gitkeep'),
            $this->file('Configuration/TsConfig/Page/Plugin/.gitkeep'),

            $this->file('Configuration/TypoScript/constants.typoscript')->mirrorFrom($autoPath)->compile($context),
            $this->file('Configuration/TypoScript/Config/Constants/DiaSite.typoscript')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TypoScript/Config/Constants/.gitkeep'),
            $this->file('Configuration/TypoScript/Helper/Constants/.gitkeep'),
            $this->file('Configuration/TypoScript/Page/Constants/.gitkeep'),
            $this->file('Configuration/TypoScript/Page/Constants/Theme.typoscript')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TypoScript/Plugin/Constants/.gitkeep'),
            $this->file('Configuration/TypoScript/ContentElement/Constants/.gitkeep'),

            $this->file('Configuration/TypoScript/setup.typoscript')->mirrorFrom($autoPath)->compile($context),
            $this->file('Configuration/TypoScript/Config/Setup/.gitkeep'),
            $this->file('Configuration/TypoScript/Helper/Setup/.gitkeep'),
            $this->file('Configuration/TypoScript/Page/Setup/.gitkeep'),
            $this->file('Configuration/TypoScript/Page/Setup/Theme.typoscript')->mirrorFrom($commonPath)->compile($context),
            $this->file('Configuration/TypoScript/Plugin/Setup/.gitkeep'),
            $this->file('Configuration/TypoScript/ContentElement/Setup/.gitkeep'),

            $this->file('Resources/Private/Language/de.locallang_be.xlf')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Private/Language/de.locallang_db.xlf')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Private/Language/de.locallang.xlf')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Private/Language/locallang_be.xlf')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Private/Language/locallang_db.xlf')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Private/Language/locallang.xlf')->mirrorFrom($commonPath)->compile($context),

            $this->file('Resources/Private/Layouts/ContentElements/.gitkeep'),
            $this->file('Resources/Private/Layouts/Page/.gitkeep'),
            $this->file('Resources/Private/Layouts/Plugin/.gitkeep'),

            $this->file('Resources/Private/Partials/ContentElements/.gitkeep'),
            $this->file('Resources/Private/Partials/Page/.gitkeep'),
            $this->file('Resources/Private/Partials/Plugin/.gitkeep'),

            $this->file('Resources/Private/Templates/ContentElements/.gitkeep'),
            $this->file('Resources/Private/Templates/Page/.gitkeep'),
            $this->file('Resources/Private/Templates/Plugin/.gitkeep'),

            $this->file('Resources/Public/Css/.gitkeep'),
            $this->file('Resources/Public/Fonts/.gitkeep'),
            $this->file('Resources/Public/PWA/.gitkeep'),
            $this->file('Resources/Public/PWA/android-chrome-192x192.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/android-chrome-512x512.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/apple-touch-icon.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/favicon-16x16.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/favicon-32x32.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/favicon.ico')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/mstile-150x150.png')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/safari-pinned-tab.svg')->mirrorFrom($commonPath),
            $this->file('Resources/Public/PWA/browserconfig.xml')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Public/PWA/site.webmanifest')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Public/Images/.gitkeep'),
            $this->file('Resources/Public/Images/logo.svg')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Public/Images/logo-inverted.svg')->mirrorFrom($commonPath)->compile($context),
            $this->file('Resources/Public/JavaScript/.gitkeep'),
            $this->file('Resources/Public/JavaScript/Src/.gitkeep'),
            $this->file('Resources/Public/JavaScript/Src/Components/.gitkeep'),
            $this->file('Resources/Public/JavaScript/Src/Services/.gitkeep'),
            $this->file('Resources/Public/JavaScript/Src/Utilities/.gitkeep'),
            $this->file('Resources/Public/JavaScript/Dist/.gitkeep'),
            $this->file('Resources/Public/Scss/.gitkeep'),
            $this->file('Resources/Public/Scss/Theme/_variables.scss')->mirrorFrom($autoPath)->compile($context),
            $this->file('Resources/Public/Scss/Theme/theme.scss')->mirrorFrom($autoPath)->compile($context),

            // JS Package
            $this->file('jsconfig.json')->mirrorFrom($autoPath)->compile($context),
            $this->file('package.json')->mirrorFrom($commonPath)->compile($context)->transform(
              function(File $file)use($isCommon){
                if($isCommon){
                  return;
                }
                $package = json_decode((string) $file,true);
                $package['devDependencies']["dia-site-common"] = "file:../dia_site_common";
                $file->setData([json_encode($package,JSON_PRETTY_PRINT)]);
              }
            ),
            $this->file('.babelrc.js')->mirrorFrom($autoPath)->compile($context),
            $this->file('.eslintignore')->mirrorFrom($commonPath)->compile($context),
            $this->file('.eslintrc.js')->mirrorFrom($autoPath)->compile($context),
            $this->file('.npmignore')->mirrorFrom($commonPath)->compile($context),
            $this->file('.prettierignore')->mirrorFrom($commonPath)->compile($context),
            $this->file('.prettierrc.js')->mirrorFrom($autoPath)->compile($context),
            $this->file('webpack.config.js')->mirrorFrom($autoPath)->compile($context),
        ];

        if($isCommon){
          $files[] = $this->file('Resources/Public/JavaScript/Src/Services/ServiceManager.js')->mirrorFrom($commonPath)->compile($context);
        }else{
          $files[] = $this->file('Resources/Public/JavaScript/Src/main.js')->mirrorFrom($customPath)->compile($context);
        }

        return $files;
    }

    protected function migratePages(ConnectionPool $pool){
        if($this->get('extensionName') === 'common') return;

        $extensionHumanName = $this->get('extensionHumanName');
        $extensionKey = $this->get('extensionKey');

        $pages = $pool->getConnectionForTable('pages');

        $pages->insert(
            'pages',
            [
                'pid' => 0,
                'crdate' => time(),
                'cruser_id' => 1,
                'tstamp' => time(),
                'title' => $extensionHumanName,
                'slug' => '/',
                'doktype' => 1,
                'backend_layout' => 'pagets__default',
                'backend_layout_next_level' => 'pagets__default',
                'tsconfig_includes' => implode(',', [
                    'EXT:dia_ge_bootstrap/Configuration/TsConfig/Page/ContentElement/AllGrid.tsconfig',
                    'EXT:dia_site/Configuration/TsConfig/Page.tsconfig',
                    'EXT:dia_site_common/Configuration/TsConfig/Page.tsconfig',
                    'EXT:' . $extensionKey . '/Configuration/TsConfig/Page.tsconfig',
                ]),
                'is_siteroot' => 1,
                'perms_userid' => 1,
                'perms_groupid' => 1,
                'perms_user' => 31,
                'perms_group' => 31,
                'perms_everybody' => 1
            ]
        );

        $rootPageUid = $pages->lastInsertId('pages');

        // 404
        $pages->insert(
            'pages',
            [
                'pid' => $rootPageUid,
                'crdate' => time(),
                'cruser_id' => 1,
                'tstamp' => time(),
                'title' => '404',
                'slug' => '/404',
                'doktype' => 1,
                'nav_hide' => 1,
                'perms_userid' => 1,
                'perms_groupid' => 1,
                'perms_user' => 31,
                'perms_group' => 31,
                'perms_everybody' => 1
            ]
        );
        $errorPageUid = $pages->lastInsertId('pages');

        // Storage
        $pages->insert(
            'pages',
            [
                'pid' => $rootPageUid,
                'crdate' => time(),
                'cruser_id' => 1,
                'tstamp' => time(),
                'title' => 'Storage',
                'doktype' => 254,
                'perms_userid' => 1,
                'perms_groupid' => 1,
                'perms_user' => 31,
                'perms_group' => 31,
                'perms_everybody' => 1
            ]
        );

        $storagePageUid = $pages->lastInsertId('pages');

        // ROOT TEMPLATE
        $pool->getConnectionForTable('sys_template')->insert(
            'sys_template',
            [
                'pid' => $rootPageUid,
                'crdate' => time(),
                'cruser_id' => 1,
                'tstamp' => time(),
                'title' => 'Main TypoScript Rendering',
                'sitetitle' => $extensionHumanName,
                'root' => 1,
                'clear' => 1,
                'include_static_file' => implode(',', [
                    'EXT:dia_site/Configuration/TypoScript',
                    'EXT:dia_site_common/Configuration/TypoScript',
                    'EXT:' . $extensionKey . '/Configuration/TypoScript',
                ]),
                'constants' => '',
                'config' => '',
                'description' => ''
            ]
        );

        $this->set([
            'rootPageUid' => $rootPageUid,
            'errorPageUid' => $errorPageUid,
            'storagePageUid' => $storagePageUid
        ]);
    }

    /**
     * @param string $extensionName
     * @param string $extensionKey
     * @throws NoSuchCacheException
     */
    protected function createSiteConfiguration()
    {
        $extensionName = $this->get('extensionName');
        $siteConfiguration = $this->getSiteConfiguration();
        $allExistingSites = $siteConfiguration->getAllExistingSites();

        if(isset($allExistingSites[$extensionName])) return;

        $site = [
            'rootPageId' => (int)$this->get('rootPageUid'),
            'base' => '/',
            'languages' => [
                [
                    'title' => 'Deutsch',
                    'enabled' => true,
                    'languageId' => 0,
                    'base' => '/',
                    'typo3Language' => 'de',
                    'locale' => 'de_DE.UTF-8',
                    'iso-639-1' => 'de',
                    'navigationTitle' => 'Deutsch',
                    'hreflang' => 'de-DE',
                    'direction' => 'ltr',
                    'flag' => 'de',
                ],
                [
                    'title' => 'English',
                    'enabled' => false,
                    'languageId' => 1,
                    'base' => '/en/',
                    'typo3Language' => 'default',
                    'locale' => 'en_US.UTF-8',
                    'iso-639-1' => 'en',
                    'navigationTitle' => 'English',
                    'hreflang' => 'en-us',
                    'direction' => 'ltr',
                    'flag' => 'us',
                ],
            ],
            'baseVariants' => [
                [
                    'base' => $extensionName . '.ddev.site',
                    'condition' => 'applicationContext == "Development"',
                ]
            ],
            'errorHandling' => [
                [
                    'errorCode' => '404',
                    'errorHandler' => 'Page',
                    'errorContentSource' => 't3://page?uid=' . $this->get('errorPageUid'),
                ],
            ],
            'routes' => [
                [
                    'route' => 'robots.txt',
                    'type' => 'staticText',
                    'content' => "User-agent: *\r\nDisallow: /typo3/\r\nDisallow: /typo3_src/",
                ],
                [
                    'route' => 'sitemap.xml',
                    'type' => 'uri',
                    'source' => 't3://page?type=900',
                ],
            ],
        ];

        $siteConfiguration->write($extensionName, $site);

    }
}
