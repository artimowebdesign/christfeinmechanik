<?php
namespace Dia\DiaSite\Boilerplate;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;

abstract class AbstractBoilerplate {

    /**
     * @var array
     */
    protected $context = null;

    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    public function __construct(
        ConnectionPool $connectionPool
    ){
        $this->connectionPool = $connectionPool;
    }

    abstract protected function getFiles():array;

    abstract protected function getMigrations():array;

    public function getContext():array {
        if(!is_array($this->context)){
            $this->context = [];
        }

        return $this->context;
    }

    public function get(string $key,$fallback=null){
        if(is_array($this->context) && array_key_exists($key,$this->context)){
            return $this->context[$key];
        }
        return $fallback;
    }

    public function set($key, $value = null):AbstractBoilerplate {
        if(!is_array($this->context)){
            $this->context = [];
        }

        if(is_array($key)){
            foreach($key as $_key => $_value){
                $this->set($_key,$_value);
            }
        }else if(is_string($key)){
            $this->context[$key] = $value;
        }else{
            throw new \Exception('invalid key');
        }

        return $this;
    }

    protected function file($filePath,?string $type = File::class):File {
        if(is_null($type)){
            throw new \Exception('unknown file type');
        }

        $file = new $type();

        if(!($file instanceof File)){
            throw new \Exception('file type not of type File');
        }

        $file->setPath($filePath);

        return $file;
    }

    protected function createContext(?array $overrides = []):array {
        $context = $overrides;

        $extensionName = $context['extensionName'] ?? 'untitled';
        $extensionKey = $context['extensionKey'] ?? $this->getExtensionKeyForName($extensionName);
        $packageName = $context['packageName'] ?? $this->getPackageNameForName($extensionName);
        $packagesPath = $context['packagesPath'] ?? null;
        $basePath = $context['basePath'] ?? null;
        $extensionHumanName = $context['extensionHumanName'] ?? $this->getExtensionHumanNameForName($extensionName);

        if($packagesPath && !$basePath){
            $basePath = rtrim($packagesPath,DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $extensionKey;
        }

        $context = array_merge($context,[
            'extensionName' => $extensionName,
            'extensionKey' => $extensionKey,
            'packageName' => $packageName,
            'packagesPath' => $packagesPath,
            'basePath' => $basePath,
            'extensionHumanName' => $extensionHumanName
        ]);

        return $context;
    }

    protected function getExtensionKeyForName(string $extensionName):string {
        $key = str_replace('-','_',$extensionName);
        return "dia_$key";
    }

    protected function getPackageNameForName(string $extensionName):string {
        return "dia/dia-$extensionName";
    }

    protected function getExtensionHumanNameForName(string $extensionName):string {
      return implode(' ',
        array_map(
          'ucfirst',
          explode('_',$this->getExtensionKeyForName($extensionName))
        )
      );
    }

    public function execute(array $context = null,callable $progressCallback = null){
        $this->context = $this->createContext($context);

        if(method_exists($this,'prepare')){
            call_user_func([$this,'prepare']);
        }

        $this->writeFiles($progressCallback);

        if(method_exists($this,'postWriteFiles')){
            call_user_func([$this,'postWriteFiles']);
        }

        $this->migrate($progressCallback);

        if(method_exists($this,'postMigrate')){
            call_user_func([$this,'postMigrate']);
        }
    }

    protected function writeFiles(callable $progressCallback = null){
        $basePath = $this->get('basePath');
        $files = $this->getFiles();
        foreach($files as $file){
            $this->writeFile($file, $basePath);

            if(!is_null($progressCallback)){
                call_user_func($progressCallback, "write files", $file,$files);
            }
        }
    }

    protected function writeFile(File $file, ?string $basePath = null){
        $filePath = $file->getPath();

        if(strpos($filePath,DIRECTORY_SEPARATOR) !== 0 && !is_null($basePath)){
            $filePath = rtrim($basePath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $filePath;
        }

        if(is_file($filePath) && !$this->overwriteFiles){
            throw new \Exception("$filePath already exists");
        }

        $pathinfo = pathinfo($filePath);

        GeneralUtility::mkdir_deep($pathinfo['dirname']);

        file_put_contents(
            $filePath,
            (string)$file,
            $file->getFlags()
        );
    }

    protected function migrate(callable $progressCallback = null){
        $migrations = $this->getMigrations();

        foreach($this->getMigrations() as $migration){

            if(is_callable($migration)){
                call_user_func($migration,$this->connectionPool);
            }else if(is_object($migration) && method_exists($migration,'up')){
                $migration->up($this->connectionPool);
            }

            if(!is_null($progressCallback)){
                call_user_func($progressCallback, "migrate", $migration,$migrations);
            }
        }
    }
}
