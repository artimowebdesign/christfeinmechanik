<?php
namespace Dia\DiaSite\Boilerplate;

class File {

    /**
     * @var bool
     */

    protected $pretty = true;

    /**
     * @var string
     */
    protected $path = null;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var int
     */
    protected $flags = 0;

    public function __construct(?string $path = null)
    {
        $this->path = $path;
    }

    public function getPretty():bool{
        return $this->pretty;
    }

    public function setPretty(bool $pretty):File {
        $this->pretty = $pretty;
        return $this;
    }

    public function getPath():string{
        return $this->path;
    }

    public function setPath(string $path):File {
        $this->path = $path;
        return $this;
    }

    public function getData():array{
        return $this->data;
    }

    public function setData(array $data):File {
        $this->data = $data;
        return $this;
    }

    public function getFlags():int{
        return $this->flags;
    }

    public function setFlags(int $flags):File {
        $this->flags = $flags;
        return $this;
    }

    public function append(...$contents):File {
        foreach($contents as $content){
            $this->data []= $content;
        }
        return $this;
    }

    public function appendFiles(...$paths):File {
        $files = array_map(function($filePath){return file_get_contents($filePath);},$paths);
        return $this->append(...$files);
    }

    public function mirrorFrom($boilerplateResourcesPath):File {
        return $this->appendFiles($boilerplateResourcesPath . DIRECTORY_SEPARATOR . $this->getPath());
    }

    public function compile(array $context):File {

        foreach($context as $key => $contextValue){
            $regex = '/' . preg_quote("{%") . '(\s*)(' . preg_quote($key) . ')(\s*)' . preg_quote("%}") . '/';
            foreach($this->data as &$value){
                if(is_string($value)){
                    $value = preg_replace($regex,(string)$contextValue,$value);
                }
            }
        }

        return $this;
    }

    public function assign(string $varname, $value):File{
        return $this->append("$varname = ".$value."\n");
    }

    public function assignMultiple(array $data):File{
        foreach($data as $varname => $value){
            $this->assign($varname,$value);
        }
        return $this;
    }

    public function transform(callable $transformFn):File {
      call_user_func($transformFn, $this);
      return $this;
    }

    public function __toString(){
        return implode($this->pretty?"\n":"",$this->data);
    }
}
