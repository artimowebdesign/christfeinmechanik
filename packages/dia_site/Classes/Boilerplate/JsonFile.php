<?php
namespace Dia\DiaSite\Boilerplate;

class JsonFile extends File {
    public function __toString() {
        return json_encode($this->data,$this->pretty ? JSON_PRETTY_PRINT : 0);
    }

    public function assign(string $varname, $value):File{
        if(!is_array($this->data)){
            $this->data = [];
        }

        $this->data[$varname] = $value;

        return $this;
    }
}
