<?php
namespace Dia\DiaSite\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extensionmanager\Exception\ExtensionManagerException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Dia\DiaSite\Boilerplate\SitePackageBoilerplate;

class GenerateCommand extends Command
{
    public static $boilerplates = [
        'SitePackage' => SitePackageBoilerplate::class
    ];

    /**
     * @var OutputInterface|\Symfony\Component\Console\Style\StyleInterface
     */
    private $output;

    /**
     * @var InputInterface
     */
    private $input;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setDescription('Create extensions based on a boilerplate')
            ->setHelp('Create extensions based on a boilerplate' . LF . 'If you want to get more detailed information, use the --verbose option.')
            ->addArgument(
                'extensionNames',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'Name of your Extension to create, only allowed a-z, 0-9 and -'
            );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws NoSuchCacheException
     * @throws ExtensionManagerException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = new SymfonyStyle($input, $output);
        $this->input = $input;

        $extensionNames = $input->getArgument('extensionNames');

        $packagesPath = $this->output->ask(
            'place extensions in the following directory',
            $this->getDefaultPackagesPath(),
            function($value){
                return is_dir($value)?$value:null;
            }
        );

        if(is_null($packagesPath)){
            $this->output->error("$packagesPath is not a directory");
            return 1;
        }

        $boilerplateKey = $this->output->choice(
            'which boilerplate should be used?',
            array_keys(self::$boilerplates),
            key(self::$boilerplates)
        );

        $boilerplateClass = self::$boilerplates[$boilerplateKey];

        $boilerplate = GeneralUtility::makeInstance(ObjectManager::class)->get($boilerplateClass);

        $currentOperation = null;
        $progressCallback = function($operation, $processedObject, $queue) use (&$currentOperation){
            $total = count($queue);
            $index = array_search($processedObject,$queue);

            if($operation !== $currentOperation){
                $currentOperation = $operation;
                $this->output->note("running $operation");
                $this->output->progressStart($total);
            }

            $this->output->progressAdvance(1);

            if($index + 1 === $total){
                $this->output->progressFinish();
            }
        };

        $packagesToInstall = [];

        foreach($extensionNames as $extensionName) {
            $boilerplate->execute(
                [
                    'extensionName' => $extensionName,
                    'packagesPath' => $packagesPath,
                ],
                $progressCallback
            );

            $packageName = $boilerplate->get('packageName');

            if($packageName){
                $packagesToInstall[] = [$packageName];
            }
        }

        if($this->output->confirm('install newly created packages?')){
            $this->composerRequire($packagesToInstall);
        }

        return 0;
    }

    protected function spawn(array $command, string $cwd = null, array $env = null, $input = null, ?float $timeout = 600){
        $process = new Process($command,$cwd,$env,$input,$timeout);
        $process->run(function ($type, $buffer) {
            $this->output->write($buffer);
        });

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

    protected function composerRequire(array $packages){
        $args = ['composer','require'];
        foreach($packages as $package){
            list($packageName,$version) = $package;
            $args []= $version?"$packageName:$version":$packageName;
        }

        $this->spawn($args);
    }

    /**
     * @return string
     */
    protected function getDefaultPackagesPath():string{
        return getcwd() . DIRECTORY_SEPARATOR . 'packages';
    }
}
