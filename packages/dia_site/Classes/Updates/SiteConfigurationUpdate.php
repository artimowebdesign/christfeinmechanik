<?php
namespace Dia\DiaSite\Updates;

use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\RepeatableInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class SiteConfigurationUpdate implements UpgradeWizardInterface, RepeatableInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'diaSiteConfigurationUpdate';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return '[DIA Site] Site Configuration Update';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return '';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     */
    public function executeUpdate(): bool
    {

    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     */
    public function updateNecessary(): bool
    {
        $siteConfiguration = GeneralUtility::makeInstance(
            SiteConfiguration::class,
            Environment::getConfigPath() . '/sites'
        );

        $allExistingSites = $siteConfiguration->getAllExistingSites();
//        dump($allExistingSites);
//        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
//            ->getQueryBuilderForTable('sys_template');
//        $queryBuilder->getRestrictions()->removeAll()
//            ->add(GeneralUtility::makeInstance(DeletedRestriction::class))
//            ->add(GeneralUtility::makeInstance(HiddenRestriction::class));
//        $elementCount = $queryBuilder->count('uid')
//            ->from('sys_template')
//            ->where(
//                $queryBuilder->expr()->isNotNull('sys_template.constants'),
//                $queryBuilder->expr()->isNotNull('sys_template.config'),
//                $queryBuilder->expr()->eq('root', 1)
//            )
//            ->execute()->fetchColumn(0);
        return false;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }
}
