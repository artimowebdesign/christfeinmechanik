<?php

namespace Dia\DiaSite\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\RepeatableInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class AbstractTableUpdate
 * @package Dia\DiaSite\Updates
 */
abstract class AbstractTableUpdate implements UpgradeWizardInterface, RepeatableInterface
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var array
     */
    protected $tableReplacements = [];

    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title ?? $this->identifier;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function executeUpdate(): bool
    {
        $returns = [];
        foreach ($this->tableReplacements as $table => $replacements) {
            $returns[] = $this->executeTableUpdate($table, $replacements);
        }
        return in_array(true, $returns);
    }

    /**
     * @inheritDoc
     */
    public function updateNecessary(): bool
    {
        $returns = [];
        foreach ($this->tableReplacements as $table => $replacements) {
            $returns[] = $this->updateTableNecessary($table, $replacements);
        }
        return in_array(true, $returns);
    }

    /**
     * @inheritDoc
     */
    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }

    /**
     * @param string $table
     * @param array $replacements
     * @return bool
     */
    public function executeTableUpdate(string $table, array $replacements): bool
    {
        if (!$this->updateTableNecessary($table, $replacements)) return true;

        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll();
        $statement = $queryBuilder->select('*')
            ->from($table)
            ->orWhere(...$this->createQueryOrExpression($queryBuilder, $replacements))
            ->execute();

        while ($record = $statement->fetch()) {
            $overrideFields = [];
            foreach ($replacements as $searchField => $replacement) {
                foreach ($replacement as $searchValue => $replace) {
                    if ((string)$record[$searchField] === (string)$searchValue) {
                        foreach ($replace as $field => $value) {
                            $overrideFields[$field] = $value;
                        }
                    }
                }
            }

            if (!empty($overrideFields)) {
                $queryBuilder = $connection->createQueryBuilder();
                $queryBuilder->update($table)
                    ->where(
                        $queryBuilder->expr()->eq(
                            'uid',
                            $queryBuilder->createNamedParameter($record['uid'], \PDO::PARAM_INT)
                        )
                    );
                foreach ($overrideFields as $field => $value) {
                    $queryBuilder->set($field, $value);
                }
                $queryBuilder->execute();
            }
        }
        return true;
    }

    /**
     * @param string $table
     * @param array $replacements
     * @return bool
     */
    public function updateTableNecessary(string $table, array $replacements): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $queryBuilder->getRestrictions()->removeAll();

        $countQuery = $queryBuilder->count('uid')
            ->from($table)
            ->orWhere(...$this->createQueryOrExpression($queryBuilder, $replacements));

        return (bool)$countQuery->execute()->fetchColumn(0);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $replacements
     * @return array
     */
    protected function createQueryOrExpression(QueryBuilder $queryBuilder, array $replacements): array
    {
        $orWhereExpression = [];
        foreach ($replacements as $field => $replacement) {
            $orWhereExpression[] = $queryBuilder->expr()->in(
                $field,
                $queryBuilder->createNamedParameter(
                    array_keys($replacement),
                    Connection::PARAM_STR_ARRAY
                )
            );
        }
        return $orWhereExpression;
    }
}
