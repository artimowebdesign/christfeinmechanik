<?php
declare(strict_types=1);

namespace Dia\DiaSite\Utility;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FileUtility
 * @package Dia\DiaSite\Traits
 */
class FileUtility
{
    /**
     * @param string $extensionKey
     * @return string
     */
    public static function getExtensionsPath(string $extensionKey = ''): string
    {
        $path = [Environment::getExtensionsPath()];
        if (!empty($extensionKey)) {
           $path[] = $extensionKey;
        }
        return implode(DIRECTORY_SEPARATOR, $path);
    }

    /**
     * @param array $setup
     * @param int $deep
     * @return string
     */
    public static function formatTypoScript(array $setup, int $deep = 0): string
    {
        $output = '';
        foreach ($setup as $key => $value) {
            if (is_array($value)) {
                $output .= str_repeat("    ", $deep) . $key . ' {' . "\n";
                $output .= self::formatTypoScript($value, $deep + 1);
                $output .= str_repeat("    ", $deep) . '}' . "\n";
            } else {
                $output .= str_repeat("    ", $deep) . $key . ' = ' . $value . "\n";
            }
        }
        return $output;
    }

    /**
     * @param $content
     * @param $path
     * @param bool $override
     * @return bool
     */
    public static function writeFile(string $content, string $path, bool $override = true): bool
    {
        if (!is_file($path)) {
            $pathInfo = pathinfo($path);
            GeneralUtility::mkdir_deep($pathInfo['dirname']);
        }
        if (!is_file($path) || $override) {
            return GeneralUtility::writeFile($path, $content);
        } else {
            return false;
        }
    }

    /**
     * @param array $setup
     * @param $path
     * @param bool $override
     * @return bool
     */
    public static function writeTypoScriptFile(array $setup, string $path, bool $override = true): bool
    {
        return self::writeFile(self::formatTypoScript($setup), $path, $override);
    }
}
