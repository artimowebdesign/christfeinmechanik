<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace Dia\DiaSite\Frontend\ContentObject;


/**
 * Contains CONTENT class object.
 */
class FluidTemplateContentObject extends \TYPO3\CMS\Frontend\ContentObject\FluidTemplateContentObject
{
    /**
     * Rendering the cObject, CONTENT
     *
     * @param array $conf Array of TypoScript properties
     * @return string Output
     */
    public function render($conf = [])
    {
      if($this->cObj){
        $data = $this->cObj->data;
        if($data['CType'] && $data['layout'] && $conf && $conf['extbase.']['controllerExtensionName'] === 'dia_site'){
          $customTemplateName = \Dia\DiaSite\TCA\ContentLayoutProcessor::getTemplateName($data['CType'],$data['layout']);
          if($customTemplateName){
            $conf['templateName'] = $customTemplateName;
          }
        }
      }

      return parent::render($conf);
    }
}
