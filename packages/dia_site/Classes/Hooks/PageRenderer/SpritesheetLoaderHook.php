<?php

/*
 * This file is part of the package dia/dia-site.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Dia\DiaSite\Hooks\PageRenderer;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * SpritesheetLoaderHook
 */
class SpritesheetLoaderHook
{

    /**
     * @var string
     */
    protected $tempDirectory = 'typo3temp/assets/spritesheet/';

    /**
     * @param array $params
     * @param PageRenderer $pagerenderer
     */
    public function execute(&$params, &$pagerenderer)
    {
      if (TYPO3_MODE !== 'FE' ||
        !((bool) $this->getTypoScriptConstant('page.theme.spritesheet.enable'))
      ) {
        return;
      }

      $sprites = [];
      foreach($this->getTemplateService()->setup_constants['page.']['theme.']['spritesheet.']['files.'] ?? [] as $pattern){
        $sprites = array_merge($sprites,$this->resolveInternalGlob($pattern));
      }

      $sprites = array_filter($sprites);

      if(!$sprites) return;

      $source = $this->generateSpritesheet($sprites);

      if(empty($source)) return;

      $filename = $this->getTyposcriptFrontendController()->site->getIdentifier() . '.svg';

      switch($this->getTypoScriptConstant('page.theme.spritesheet.mode')){
        case 'inline':
          $params['footerData'][] = $source;
          break;
        case 'xhr':
        default:
          $cacheId = '?v='.hash('ripemd160',$source);
          $params['headerData'][] = $this->generateXhrLoaderScript($this->writeAsset($source,$filename) . $cacheId);
      }
    }

    protected function generateSpritesheet($sprites){
      $document = '<svg style="display:none;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">';
      $viewOffset = 0;

      foreach($sprites as $sprite){
        $id = pathinfo($sprite, PATHINFO_FILENAME);
        $str = trim(file_get_contents($sprite));
        $str = '<symbol id="'.$id.'" ' . substr($str, 4, strlen($str) - 8) . 'symbol>';
        $symbol = simplexml_load_string($str);
        $width = 1;
        $height = 1;
        if(isset($symbol['width'])){
          $width = (int) $symbol['width'];
          unset($symbol['width']);
        }
        if(isset($symbol['height'])){
          $height = (int) $symbol['height'];
          unset($symbol['height']);
        }

        foreach($this->getTemplateService()->setup_constants['page.']['theme.']['spritesheet.']['symbolAttributes.'] ?? [] as $key => $value){
          $symbol->addAttribute($key,$value);
        }

        $document .= trim(preg_replace("/\<\?.*\?\>/","",$symbol->asXML()));
        $document .= '<use xlink:href="#'.$id.'" width="'.$width.'" height="'.$height.'" x="0" y="'.$viewOffset.'" id="'.$id.'-instance"></use>';
        $document .= '<view id="'.$id.'-view" viewBox="0 '.$viewOffset.' '.$width.' '.$height.'"></view>';
        $viewOffset+= $height;
      }

      $document .= "</svg>";

      return $document;
    }

    protected function generateXhrLoaderScript($assetPath){
      $js = [
        '<script>',
        '((d)=>',
          "fetch('$assetPath')",
          '.then(r => r.text())',
          '.then(s => d.body.appendChild(new DOMParser().parseFromString(s, \'text/xml\').firstChild),d.documentElement.classList.add(\'spritesheet-loaded\'))',
        ')(document)',
        '</script>'
      ];
      return implode('', $js);
    }

    /**
     * @param string $source
     * @param string $prefix
     * @return string
     */
    public function writeAsset($source, string $filename = 'spritesheet.svg')
    {
        $relativePath = $this->tempDirectory . $filename;

        if (!file_exists(Environment::getPublicPath() . '/' . $this->tempDirectory)) {
            GeneralUtility::mkdir_deep(Environment::getPublicPath() . '/' . $this->tempDirectory);
        }

        file_put_contents($relativePath,$source);

        return '/' . $relativePath;
    }

    /**
     * @param string $typoscriptConstant
     * @return string
     */
    private function getTypoScriptConstant($typoscriptConstant)
    {
        if (!isset($this->getTemplateService()->flatSetup)
            || !is_array($this->getTemplateService()->flatSetup)
            || count($this->getTemplateService()->flatSetup) === 0) {
            $this->getTemplateService()->generateConfig();
        }
        if (isset($this->getTemplateService()->flatSetup[$typoscriptConstant])) {
            return $this->getTemplateService()->flatSetup[$typoscriptConstant];
        }
        return '';
    }

    /**
     * @return TypoScriptFrontendController
     */
    private function getTyposcriptFrontendController()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * @return TemplateService
     */
    private function getTemplateService()
    {
        return $this->getTyposcriptFrontendController()->tmpl;
    }

    /**
     * @param string $pattern
     * @param int $flags
     * @return array|false
     */
    protected function resolveInternalGlob(string $pattern, int $flags = 0){
      if (strpos($pattern, 'EXT:') === 0) {
        $pathSegments = explode(DIRECTORY_SEPARATOR, $pattern);
        $extKey = substr(array_shift($pathSegments),4);
        $extPath = rtrim(ExtensionManagementUtility::extPath($extKey),DIRECTORY_SEPARATOR);
        $pattern = implode(DIRECTORY_SEPARATOR,array_merge([$extPath],$pathSegments));
      }

      return glob($pattern,$flags);
    }
}
