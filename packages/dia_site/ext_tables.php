<?php
defined('TYPO3_MODE') or die();

// Register as a skin
$GLOBALS['TBE_STYLES']['skins']['dia_site'] = [
    'name' => 'dia_site',
    'stylesheetDirectories' => [
        'css' => 'EXT:dia_site/Resources/Public/Backend/Css/'
    ]
];
