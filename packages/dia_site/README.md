# DIA Site

DIA Site Extension based on bootstrap_package

## Features
### DIA Site Generator

You can generate DIA Site Extensions with the following command:

```bash
$ php bin/typo3 dia:site:generate <sitename> <sitename> <sitename>
```

for ddev:

```bash
$ ddev exec bin/typo3 dia:site:generate <sitename> <sitename> <sitename>
```

### Progressive Web App Configuration

The generator takes care of browserconfig.xml, manifest and app icon generation, simply
head over to [realfavicongenerator](https://realfavicongenerator.net/) and generate a package, and place its contents
into `packages/dia_site_yoursite/Resources/Public/PWA`, but keep the existing `browserconfig.xml`
and `site.webmanifest`, since they are already contextualized.

adjust the specifics in `packages/dia_site_yoursite/Configuration/TypoScript/Page/Constants/Theme.typoscript`, these are the defaults:

```typoscript
page.theme.pwa {
  enable = 1
  themeColor = #ffffff
  extensionName = DiaSiteCommon
  shortcutIcon = PWA/favicon.ico
  icons {
    0 {
      rel = icon
      sizes = 16x16
      type = image/png
      path = PWA/favicon-16x16.png
    }
    1 {
      rel = icon
      sizes = 32x32
      type = image/png
      path = PWA/favicon-32x32.png
    }
    2 {
      rel = apple-touch-icon
      sizes = 180x180
      type = image/png
      path = PWA/apple-touch-icon.png
    }
  }
  manifest = PWA/site.webmanifest
  maskIcon {
    enable = 1
    path = PWA/safari-pinned-tab.svg
    color = #84bd00
  }
  msapplication {
    enable = 1
    tileColor = #ffffff
    tileImage = PWA/mstile-150x150.png
    browserconfig = PWA/browserconfig.xml
  }
}
```

### Spritesheet Generation
Your Icons are concatinated into a Spritesheet, simply register them using glob patterns
in `page.theme.spritesheet.files` and use them in your templates with the svg `use` syntax:
```html
<svg class="icon">
  <use xlink:href="#arrow-up"></use>
</svg>
```

where `xlink:href` contains the filename of the icon, so `Resources/Public/Icons/my-icon.svg` becomes `xlink:href="#my-icon"`

to make the icons behave more like iconfonts, simply include this scss snippet somewhere in your theme:

```scss
.icon {
  display:inline-block;
  width:1em;
  height:1em;
  use {
    fill: none;
    stroke: currentColor;
  }
}

@mixin useIcon($iconName){
  @extend .icon;
  background-image:url('#{$spritesheet}##{$iconName}-view');
  background-size:contain;
  background-position:center center;
}

symbol.sprite {
  width:inherit;
  height:inherit;
  path {
    fill: inherit;
    stroke: inherit;
  }
}

.example-icon {
  @include useIcon('my-icon');
}

```

adjust the specifics in `packages/dia_site_yoursite/Configuration/TypoScript/Page/Constants/Theme.typoscript`, these are the defaults:

```typoscript
page.theme.spritesheet {
  enable = 1
  mode = xhr
  files {
    common = EXT:dia_site_common/Resources/Public/Icons/*.svg
  }
  symbolAttributes {
    class = sprite
    preserveaspectratio = xMidYMid meet
  }
}
```

by default the mode is set to `xhr` which includes a small js that loads the spritesheet
into the document body, you may also change this setting to `inline`, which directly inserts the spritesheet into the document during the page render.

### JS Compilation
if your project was set up using the dia boilerplate, use the builtin command `bin/dia` for js compilation
```bash
# install npm dependencies of dia_site_yoursite
$ bin/dia common install && bin/dia yoursite install

# build a site package bundle
$ bin/dia yoursite run build

# watch and build dev bundles continuously
$ bin/dia yoursite run dev
```

if you dont want to use the cli, just cd into your site package and run the npm commands yourself

```bash
$ cd packages/dia_site_yoursite && npm run your-script
```
