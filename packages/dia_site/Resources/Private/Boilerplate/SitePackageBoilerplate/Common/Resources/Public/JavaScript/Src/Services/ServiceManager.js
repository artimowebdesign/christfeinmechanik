class ServiceManager {
  constructor(services = [], autostart = true){
    this.services = services;
    autostart && this.start();
  }

  start(){
    if(document.readyState === "loading") {
      document.addEventListener('readystatechange', event => {
        if (event.target.readyState === 'interactive') {
          this.initializeServices();
        }
      });
    }else{
      this.initializeServices();
    }
  }

  async initializeServices(){
    this.destroyInstances();
    this.instances = await Promise.all(
      this.services.map(serviceFunction => serviceFunction(this))
    );
    window.dispatchEvent(
      new CustomEvent("service-manager:initialized",{detail:this})
    );
  }

  destroyInstances(){
    if(!Array.isArray(this.instances) || !this.instances.length) return;
    let instance;
    while(instance = this.instances.pop()){
      if(instance){
        const type = typeof(instance);
        if(type === 'function'){
          instance();
        }else if(type === 'object' && typeof(instance.destroy) === 'function'){
          instance.destroy();
        }
      }
    }
  }
}

export default ServiceManager;
