<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '{% extensionName %}',
    'description' => '{% extensionName %}',
    'category' => '{% category %}',
    'state' => '{% stability %}',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => '{% company %}',
    'version' => '{% version %}',
    'constraints' => [
        'depends' => [
            'typo3' => '{% typo3MinVersion %}-{% typo3MaxVersion %}',
            //'dia_site' => '9.0.0-9.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            '{% autoloadPsr4 %}' => 'Classes'
        ],
    ],
];
