const { join } = require('path');
const webpack = require('webpack');
module.exports = (mode = process.env.NODE_ENV, options) => {
  const config = require('@interaktiv/dia-scripts/webpack')(
    mode,
    options,
  );

  config.entry = {
    scripts: './Resources/Public/JavaScript/Src/main.js',
  };

  config.resolve = {
    alias: {
      '@{% extensionKey %}': join(__dirname, './Resources/Public/JavaScript/Src'),
    },
  };

  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env.ASSET_PATH': JSON.stringify(config.output.publicPath),
    }),
  );

  config.output.chunkFilename = '[name].chunk.js';

  return config;
};
