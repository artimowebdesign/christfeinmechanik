<?php

defined("TYPO3_MODE") or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    "{% extensionKey %}",
    "Configuration/TsConfig/Page.tsconfig",
    "{% extensionName %}"
);
