<?php
defined("TYPO3_MODE") or die();
// Add default RTE configuration

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['{% extensionKey %}'] = "EXT:{% extensionKey %}/Configuration/RTE/Default.yaml";
