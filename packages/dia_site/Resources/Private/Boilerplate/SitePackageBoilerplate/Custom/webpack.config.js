const { join } = require('path');
module.exports = (mode = process.env.NODE_ENV, options) => {
  const config = require('dia-site-common/webpack.config')(
    mode,
    options,
  );

  config.resolve.alias['@{% extensionKey %}'] = join(__dirname, './Resources/Public/JavaScript/Src');

  return config;
};
