import ServiceManager from '@dia_site_common/Services/ServiceManager';

new ServiceManager([
  async ()=>{
    console.log('page initialized');
    return ()=>{
      console.log('page destroyed');
    }
  }
]);
