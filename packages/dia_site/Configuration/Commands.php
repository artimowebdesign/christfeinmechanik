<?php

return [
    'dia:site:generate' => [
        'vendor' => 'dia_site',
        'class' => \Dia\DiaSite\Command\GenerateCommand::class,
        'schedulable' => false,
    ]
];
