<?php
defined('TYPO3_MODE') || die();

call_user_func(function ($extensionKey) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'DIA Site'
    );
}, 'dia_site');
