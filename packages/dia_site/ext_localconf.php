<?php
defined('TYPO3_MODE') or die();

// InstallUtility
call_user_func(function ($_EXTKEY) {
    if (TYPO3_MODE === 'BE') {
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
        $signalSlotDispatcher->connect(
            \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,
            'afterExtensionInstall',
            \Dia\Utility\InstallUtility::class,
            'afterExtensionInstall'
        );
    }
}, $_EXTKEY);

// Update Wizards
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['diaSiteConfigurationUpdate']
    = \Dia\DiaSite\Updates\SiteConfigurationUpdate::class;

$GLOBALS['TYPO3_CONF_VARS']['FE']['ContentObjects']['FLUIDTEMPLATE'] = \Dia\DiaSite\Frontend\ContentObject\FluidTemplateContentObject::class;

// Register spritesheet hook
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][\Dia\DiaSite\Hooks\PageRenderer\SpritesheetLoaderHook::class]
    = \Dia\DiaSite\Hooks\PageRenderer\SpritesheetLoaderHook::class . '->execute';
