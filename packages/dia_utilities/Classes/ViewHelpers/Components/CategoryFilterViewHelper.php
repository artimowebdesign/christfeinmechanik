<?php
namespace Dia\DiaUtilities\ViewHelpers\Components;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class CategoryFilterViewHelper.
 * For rendering a Filter for a given set of records, according to their
 * assigned sys_categories.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class CategoryFilterViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        // init basic Arguments
        parent::initializeArguments();

        $this->registerArgument('collections', 'array', 'List of sys_collection records to create a filter for', FALSE);
        $this->registerArgument('items', 'array', 'List of single records to create a filter for', FALSE);
        $this->registerArgument('table', 'string', 'Tablename, where source records are stored', FALSE);
        $this->registerArgument('parent', 'int', 'Parent category to recognize children only', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return string
     */
    public function render()
    {
        $tablename = '';
        if (isset($this->arguments['table'])) {
            $tablename = $this->arguments['table'];
        }

        $uidList = '';
        if (isset($this->arguments['collections'])) {
            $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

            foreach ($this->arguments['collections'] as $collection) {
                if ($collection['data']['folder']) {
                    $storage = $resourceFactory->getStorageObject($collection['data']['storage']);
                    $folder = $storage->getFolder($collection['data']['folder']);
                    $items = $storage->getFilesInFolder($folder);
                } else {
                    $items = $this->arguments['collection']['files'];
                }

                $subQueryBuilder = GeneralUtility::makeInstance(
                    ConnectionPool::class
                )->getConnectionForTable('sys_file_metadata')->createQueryBuilder();


                foreach ($items as $item) {
                    $result = $subQueryBuilder
                        ->select('uid')
                        ->from('sys_file_metadata')
                        ->where('file = ' . $item->getProperties()['uid'])
                        ->execute()
                        ->fetchAll()
                    ;
                    if ($result[0]['uid']) {
                        $uidList .= $result[0]['uid'] . ',';
                    }
                }
            }
        } elseif (isset($this->arguments['items'])) {
            $items = $this->arguments['items'];

            foreach ($items as $item) {
                if (is_array($item)) {
                    if (isset($item['uid'])) {
                        $uidList .= $item['uid'] . ',';
                    } else {
                        if (isset($item['data']['uid'])) {
                            $uidList .= $item['data']['uid'] . ',';
                        }
                    }
                } else {
                    if ($item->getUid()) {
                        $uidList .= $item->getUid() . ',';
                    }
                }
            }
        }

        if (!$tablename || !$uidList) {
            return 'Got an error';
            // ====
        }

        $queryBuilder = GeneralUtility::makeInstance(
            ConnectionPool::class
        )->getConnectionForTable('sys_category')->createQueryBuilder();

        $whereAddition = '';
        if (isset($this->arguments['parent']) && intval($this->arguments['parent'])) {
            $whereAddition = ' AND parent = ' . intval($this->arguments['parent']);
        }

        $categoryItems = $queryBuilder
            ->select('*')
            ->from('sys_category')
            ->join(
                'sys_category',
                'sys_category_record_mm',
                'relation',
                $queryBuilder->expr()->eq('relation.uid_local', 'sys_category.uid')
            )
            ->where('relation.tablenames = "' . $tablename . '"' .
                ' AND relation.uid_foreign IN(' . rtrim($uidList, ',') .')' . $whereAddition
            )
            ->groupBy('sys_category.uid')
            ->execute()
            ->fetchAll();

        return $this->renderItemsOutput($categoryItems);
        // ====
    } // End of render()
}
