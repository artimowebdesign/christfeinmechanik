<?php
namespace Dia\DiaUtilities\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Class SpacelessViewHelper
 *
 * Space Removal ViewHelper
 *
 * Removes redundant spaces between HTML tags while
 * preserving the whitespace that may be inside HTML
 * tags. Trims the final result before output.
 *
 * Heavily inspired by Twig's corresponding node type.
 *
 * Example:
 * <code title="Usage of dia:spaceless">
 * <dia:spaceless>
 * <div>
 *     <div>
 *         <div>text
 *
 * text</div>
 *     </div>
 * </div>
 * </dia:spaceless>
 * </code>
 *
 * <output>
 * <div><div><div>text text</div></div></div>
 * </output>
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @version 1.0.1
 */
class SpacelessViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * Flag, whether to escape output or not
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param array $arguments
     * @param \Closure $childClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $childClosure, RenderingContextInterface $renderingContext)
    {
        return trim(preg_replace('/(?:\s\s+|\t|\n|\r|\x0B)/', '', $childClosure()));
    }
}
