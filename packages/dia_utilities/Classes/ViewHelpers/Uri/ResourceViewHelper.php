<?php
namespace Dia\DiaUtilities\ViewHelpers\Uri;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Class ResourceViewHelper
 * @package Dia\DiaUtilities\ViewHelpers\Uri
 */
class ResourceViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Uri\ResourceViewHelper
{
    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return bool|string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $path = $arguments['path'];
        $extensionName = $arguments['extensionName'];

        if ($extensionName === null) {
            $extensionName = $renderingContext->getControllerContext()->getRequest()->getControllerExtensionName();
        }
        $uri = 'EXT:' . GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName) . '/Resources/Public/' . $path;
        $uri = GeneralUtility::getFileAbsFileName($uri);

        return is_file($uri) ? parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext) : false;
    }
}
