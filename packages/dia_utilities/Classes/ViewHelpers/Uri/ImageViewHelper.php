<?php
namespace Dia\DiaUtilities\ViewHelpers\Uri;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Class ImageViewHelper
 * @package Dia\DiaUtilities\ViewHelpers\Uri
 */
class ImageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Uri\ImageViewHelper
{
    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return bool|string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        try {
            return parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
        } catch (\Exception $e) {
            return false;
        }
    }
}
