<?php
namespace Dia\DiaUtilities\ViewHelpers\Abstr;

/**
 * Class AbstractDiaViewHelper.
 * Basic ViewHelper class for DIA ViewHelpers.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class AbstractDiaViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Flag, whether to escape output or not
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Shortlink to TYPO3 DebugUtility::debug function
     *
     * @param mixed         $content        The content to debug
     * @param string        $title          optional: Debug output title
     * @return void
     */
    public function debug($content, $title='')
    {
        \TYPO3\CMS\Core\Utility\DebugUtility::debug($content, $title);
    }
}
