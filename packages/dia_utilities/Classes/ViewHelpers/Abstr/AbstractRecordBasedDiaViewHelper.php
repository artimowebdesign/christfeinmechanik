<?php
namespace Dia\DiaUtilities\ViewHelpers\Abstr;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class AbstractRecordBasedDiaViewHelper.
 * Basic ViewHelper class for DIA ViewHelpers.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class AbstractRecordBasedDiaViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize basic arguments to any Record-based DIA ViewHelper
     *
     * @return void
     */
    public function initializeArguments()
    {
        // the "as" argument allows Integrators to set a custom name on the rendered content variable, if there is any
        $this->registerArgument(
            'as', 'string', 'optional: name of the variable to add to the template, defaults to "items"', FALSE
        );
    } // End of initBasicArguments()



    /**
     * Shortcut function for the output rendering, applying the "as" argument and some other common
     * tasks in this process.
     *
     * @param mixed         $items      The items to add to the View
     * @return string                   The rendered content
     */
    public function renderItemsOutput($items = NULL)
    {
        $content = '';
        if (!empty($items)) {
            $as = 'items';
            if (isset($this->arguments['as']) && $this->arguments['as']) {
                $as = $this->arguments['as'];
            }

            $this->templateVariableContainer->add($as, $items);
            $content = $this->renderChildren();
            $this->templateVariableContainer->remove($as);
        }

        return $content;
        // ====
    } // End of renderItemsOutput()
}
