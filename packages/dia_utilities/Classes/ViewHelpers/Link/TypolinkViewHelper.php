<?php
namespace Dia\DiaUtilities\ViewHelpers\Link;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * TypolinkViewHelper Logic.
 * Using the complete typolink configuration options for creating links in Fluid.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class TypolinkViewHelper extends AbstractDiaViewHelper
{
	/**
	 * Initialize Arguments
	 *
	 * @return void
	 */
	public function initializeArguments()
	{
		$this->registerArgument('parameter', 'string', 'The typolink parameter', TRUE, 0);
		$this->registerArgument('config', 'array', 'Additional typolink config', FALSE, false);
	}

	/**
	 * Render the content.
	 *
	 * @return string
	 */
	public function render()
	{
        $content = $this->arguments['value'] ?? $this->renderChildren();

        $config = $this->arguments['config'] ?? [];
        $config['parameter'] = $this->arguments['parameter'];

        /** @var ContentObjectRenderer $contentObject */
        $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $contentObject->start([], '');
		return $contentObject->typolink($content, $config);
	}
}
