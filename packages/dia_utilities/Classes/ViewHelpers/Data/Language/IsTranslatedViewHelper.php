<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Language;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class IsTranslatedViewHelper
 * Returns TRUE, if a translation for a record with the given uid exists in DB.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class IsTranslatedViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('table', 'string', 'DB table, where the record is stored', TRUE);
        $this->registerArgument('language', 'int', 'uid of the sys_language to check for translation (set -1 for determination by $_GET["L"])', FALSE);
        $this->registerArgument('uid', 'int', 'uid of the record to check for translation', FALSE);
        $this->registerArgument('gpVar', 'string', 'name of the GPVar to be used as uid', FALSE);
        $this->registerArgument('includeHidden', 'int', 'Set to 1, if hidden items should be recognized', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return int      uid of the translation record or 0 if no translation is found
     */
    public function render()
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($this->arguments['table']);

        $queryBuilder = $connection->createQueryBuilder();

        $uid = 0;
        if (isset($this->arguments['gpVar'])) {
            $segments = explode('|', $this->arguments['gpVar']);
            if (count($segments) > 1) {
                $gps = array_merge($_GET, $_POST);
                $current = $gps;
                foreach ($segments as $segment) {
                    if (isset($current[$segment])) {
                        $current = $current[$segment];
                    }
                }
                if (!empty($current)) {
                    $uid = $current;
                }
            } else {
                $uid = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP($this->arguments['gpVar']);
            }
        }
        if (isset($this->arguments['uid'])) {
            $uid = $this->arguments['uid'];
        }

        if (!$uid) {
            return 'Invalid arguments for uid determination!';
            // ====
        }

        if (!isset($this->arguments['language']) || intval($this->arguments['language']) < 0) {
            $languageId = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('L');
        } else {
            $languageId = intval($this->arguments['language']);
        }

        $parentFieldName = 'l10n_parent';
        if ($this->arguments['table'] == 'pages_language_overlay') {
            if ($languageId == 0) {
                return 1;
                // ====
            } else {
                $parentFieldName = 'pid';
            }
        }
        if ($this->arguments['table'] == 'tt_content') {
            $parentFieldName = 'l18n_parent';
        }


        if (isset($this->arguments['includeHidden']) && intval($this->arguments['includeHidden'])) {
            $row = $queryBuilder
                ->select('uid')
                ->from($this->arguments['table'])
                ->where(
                    $queryBuilder->expr()->eq(
                        'sys_language_uid',
                        $languageId
                    ),
                    $queryBuilder->expr()->eq(
                        $parentFieldName,
                        intval($uid)
                    ),
                    $queryBuilder->expr()->eq(
                        'deleted',
                        0
                    )
                )
                ->setMaxResults(1)
                ->execute()->fetch();
        } else {
            $row = $queryBuilder
                ->select('uid')
                ->from($this->arguments['table'])
                ->where(
                    $queryBuilder->expr()->eq(
                        'sys_language_uid',
                        $languageId
                    ),
                    $queryBuilder->expr()->eq(
                        $parentFieldName,
                        intval($uid)
                    ),
                    $queryBuilder->expr()->eq(
                        'deleted',
                        0
                    ),
                    $queryBuilder->expr()->eq(
                        'hidden',
                        0
                    )
                )
                ->setMaxResults(1)
                ->execute()->fetch();
        }

        if (is_array($row)) {
            return $row['uid'];
            // ====
        }

        return '0';
        // ====
    } // End of render()
}
