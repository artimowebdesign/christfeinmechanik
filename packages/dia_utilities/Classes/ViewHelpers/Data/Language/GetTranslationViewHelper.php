<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Language;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class GetTranslationViewHelper.
 * Fetch translation of a given record from the given table in the given language, if present.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class GetTranslationViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('table', 'string', 'DB table, where the record is stored', TRUE);
        $this->registerArgument('language', 'int', 'uid of the sys_language to check for translation', TRUE);
        $this->registerArgument('uid', 'int', 'uid of the record to check for translation', FALSE);
        $this->registerArgument('gpVar', 'string', 'name of the GPVar to be used as uid', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return int      uid of the translation record or 0 if no translation is found
     */
    public function render()
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($this->arguments['table']);
        $queryBuilder = $connection->createQueryBuilder();

        $parentFieldName = 'l10n_parent';
        if ($this->arguments['table'] == 'pages_language_overlay') {
            $parentFieldName = 'pid';
        }
        if ($this->arguments['table'] == 'tt_content') {
            $parentFieldName = 'l18n_parent';
        }

        $uid = $this->arguments['uid'];

        $row = $queryBuilder
            ->select('*')
            ->from($this->arguments['table'])
            ->where(
                $queryBuilder->expr()->eq(
                    'sys_language_uid',
                    intval($this->arguments['language'])
                ),
                $queryBuilder->expr()->eq(
                    $parentFieldName,
                    intval($uid)
                )
            )
            ->setMaxResults(1)
            ->execute()->fetch();


        return $this->renderItemsOutput($row);
        // ====
    } // End of render()
}
