<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Fal;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class CollectionViewHelper.
 * For fetching SysFile records from a Collection record.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class CollectionViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        // init basic Arguments
        parent::initializeArguments();

        $this->registerArgument('collection', 'mixed', 'The sys_collection uid', TRUE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return string
     */
    public function render()
    {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        $collection = $this->arguments['collection'];

        if ($collection['data']['folder']) {
            $storage = $resourceFactory->getStorageObject($collection['data']['storage']);
            $folder = $storage->getFolder($collection['data']['folder']);
            $files = $storage->getFilesInFolder($folder);
        } else {
            $files = $this->arguments['collection']['files'];
        }

        return $this->renderItemsOutput($files);
        // ====
    } // End of render()
}
