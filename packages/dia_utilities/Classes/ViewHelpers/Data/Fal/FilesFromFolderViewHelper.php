<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Fal;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class FilesFromFolderViewHelper.
 * Fetches Fal Files from a storage folder and returns the SysFile objects.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class FilesFromFolderViewHelper extends AbstractDiaViewHelper
{
    /**
     * Flag, whether to escape output or not
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        // init basic Arguments
        parent::initializeArguments();

        $this->registerArgument('folder', 'string', 'full path to the folder', TRUE);
        $this->registerArgument('storageId', 'int', 'optional: id of the storage', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content
     *
     * @return string
     */
    public function render()
    {
        $storageUid = 1;
        if (isset($this->arguments['storageId'])) {
            $storageUid = $this->arguments['storageId'];
        }
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $storage = $resourceFactory->getStorageObject($storageUid);

        $foldername = str_replace($storageUid . ':/', '', $this->arguments['folder']);
        $folderObject = $storage->getFolder($foldername);
        $files = $storage->getFilesInFolder($folderObject);

        return $this->renderItemsOutput($files);
        // ====
    } // End of render()
}
