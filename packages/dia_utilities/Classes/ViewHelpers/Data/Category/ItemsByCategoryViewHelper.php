<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Category;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class ItemsByCategoryViewHelper
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class ItemsByCategoryViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        // init basic Arguments
        parent::initializeArguments();

        $this->registerArgument('categoryUid', 'int', 'uid of the category to fetch records from', TRUE);
        $this->registerArgument('tablename', 'string', 'optional: DB table, where the records are stored', TRUE);
        $this->registerArgument('fieldname', 'string', 'optional: fieldname of the records table, storing category relations', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return string
     */
    public function render()
    {
        $as = (!empty($this->arguments['as'])) ? $this->arguments['as'] : 'items';
        $queryBuilder = GeneralUtility::makeInstance(
            ConnectionPool::class
        )->getConnectionForTable($this->arguments['tablename'])->createQueryBuilder();

        $whereAddition = '';
        if (isset($this->arguments['fieldname']) && $this->arguments['fieldname']) {
            $whereAddition = ' AND fieldname = "' . $this->arguments['fieldname'] . '"';
        }

        $results = $queryBuilder
            ->select('*')
            ->from($this->arguments['tablename'])
            ->join(
                $this->arguments['tablename'],
                'sys_category_record_mm',
                'relation',
                $queryBuilder->expr()->eq('relation.uid_local', $this->arguments['categoryUid'])
            )
            ->where('relation.tablenames = "' . $this->arguments['tablename'] . '" AND relation.uid_foreign = ' .
                $this->arguments['tablename'] . '.uid' . $whereAddition
            )
            ->orderBy('relation.sorting')
            //->groupBy($this->arguments['tablename'] . '.uid')
            ->execute()
            ->fetchAll();

        return $this->renderItemsOutput($results);
        // ====
    } // End of render()
}
