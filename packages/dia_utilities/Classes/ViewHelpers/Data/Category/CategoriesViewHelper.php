<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\Category;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class CategoriesViewHelper.
 * Fetches all sys_category Records related to a given Record.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class CategoriesViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('uid', 'int', 'uid of the record to fetch categories', TRUE);
        $this->registerArgument('table', 'string', 'DB table, where the record is stored', TRUE);
        $this->registerArgument('parent', 'int', 'optional: only categories having this parent', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return string
     */
    public function render()
    {
        $recordUid = intval($this->arguments['uid']);

        if (!$recordUid) {
            return '';
            // ====
        }

        // for fetching sys_file_metadata category relations, we need to get the
        // sys_file_metadata.uid according to the sys_file.uid given
        if ($this->arguments['table'] == 'sys_file_metadata') {
            $subQueryBuilder = GeneralUtility::makeInstance(
                ConnectionPool::class
            )->getConnectionForTable('sys_file_metadata')->createQueryBuilder();

            $result = $subQueryBuilder
                ->select('uid')
                ->from('sys_file_metadata')
                ->where('file = ' . $recordUid)
                ->execute()
                ->fetchAll()
            ;

            if ($result[0]['uid']) {
                $recordUid = $result[0]['uid'];
            }
        }

        $queryBuilder = GeneralUtility::makeInstance(
            ConnectionPool::class
        )->getConnectionForTable('sys_category')->createQueryBuilder();

        $whereAddition = '';
        if (isset($this->arguments['parent']) && intval($this->arguments['parent'])) {
            $whereAddition = ' AND parent = ' . intval($this->arguments['parent']);
        }

        $results = $queryBuilder
            ->select('*')
            ->from('sys_category')
            ->join(
                'sys_category',
                'sys_category_record_mm',
                'relation',
                $queryBuilder->expr()->eq('relation.uid_local', 'sys_category.uid')
            )
            ->where('relation.tablenames = "' . $this->arguments['table'] . '"' .
                ' AND relation.uid_foreign = ' . $recordUid . $whereAddition
            )
            ->execute()
            ->fetchAll();

        return $this->renderItemsOutput($results);
        // ====
    } // End of render()
}
