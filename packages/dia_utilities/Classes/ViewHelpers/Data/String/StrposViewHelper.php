<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\String;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class StrposViewHelper.
 * Implements php strpos function and returns the first occurence of a needle inside
 * a given haystack.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class StrposViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('haystack', 'string', 'string to search in', TRUE);
        $this->registerArgument('needle', 'string', 'substring to be looked for', TRUE);
        $this->registerArgument('offset', 'int', 'optional: offset for beginning of search', FALSE);
        $this->registerArgument('fromEnd', 'bool', 'optional: set to TRUE, if start searching from right to left (strrpos)', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content
     *
     * @return string
     */
    public function render()
    {
        $offset = 0;
        if (isset($this->arguments['offset']) && $this->arguments['offset']) {
            $offset = $this->arguments['offset'];
        }

        if (isset($this->arguments['fromEnd']) && $this->arguments['fromEnd']) {
            return strrpos($this->arguments['haystack'], $this->arguments['needle'], $offset);
            // ====
        } else {
            return strpos($this->arguments['haystack'], $this->arguments['needle'], $offset);
            // ====
        }
    } // End of render()
}
