<?php
namespace Dia\DiaUtilities\ViewHelpers\Data\String;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractRecordBasedDiaViewHelper;

/**
 * Class ExplodeViewHelper.
 * Explodes a given string to an array for further treatment.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class ExplodeViewHelper extends AbstractRecordBasedDiaViewHelper
{
    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('value', 'string', 'the string to be exploded', TRUE);
        $this->registerArgument('delimiter', 'string', 'The delimiter to be used for exploding, defaults to ","', FALSE);
    } // End of initializeArguments()



    /**
     * Main function of the ViewHelper.
     *
     * @return string
     */
    public function render()
    {
        // default delimiter
        $delimiter = ',';
        if (!empty($this->arguments['delimiter'])) {
            $delimiter = $this->arguments['delimiter'];
        }

        $parts = explode($delimiter, $this->arguments['value']);

        return $this->renderItemsOutput($parts);
        // ====
    } // End of render()
}
