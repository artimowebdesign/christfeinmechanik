<?php
namespace Dia\DiaUtilities\ViewHelpers\Html;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\PageRenderer;

/**
 * Class HeadViewHelper.
 * For adding HeaderData to the page rendering process.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class HeadViewHelper extends AbstractViewHelper
{
    /**
     * Add Header Data
     *
     * @return string
     */
    public function render()
    {
        $PageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $PageRenderer->addHeaderData($this->renderChildren());

        return '';
    }
}
