<?php
namespace Dia\DiaUtilities\ViewHelpers\Asset;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Frontend\Page\PageGenerator;

/**
 * Class StyleViewHelper.
 * For adding StyleSheet assets to the page.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @version 1.0.0
 */
class StyleViewHelper extends AbstractAssetViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('rel', 'string', '', false, 'stylesheet');
        $this->registerArgument('media', 'string', '', false, 'all');
        $this->registerArgument('title', 'string', '', false, '');
        $this->registerArgument('lib', 'bool', '', false, FALSE);
        $this->registerArgument('inline', 'bool', '', false, FALSE);
    }

    /**
     * Add CSS file
     */
    public function render()
    {
        $name = $this->arguments['name'];
        $file = $this->arguments['file'];

        if (empty($file)) {
            $block = trim($this->renderChildren());
            $name = empty($name) ? md5($block) : $name;

            if(!empty($block))
            {
                if ($this->arguments['inline'] && empty($GLOBALS['TSFE']->config['config']['inlineStyle2TempFile'])) {
                    $this->pageRenderer->addCssInlineBlock($name, $block, $this->arguments['compress'], $this->arguments['forceOnTop']);
                } else {
                    $file = GeneralUtility::writeStyleSheetContentToTemporaryFile($block);
                }
            }
        } else {
            if (strpos($file, 'EXT:') === 0) {
                $file = GeneralUtility::getFileAbsFileName($file);
                // as the path is now absolute, make it "relative" to the current script to stay compatible
                $file = PathUtility::getRelativePathTo($file);
                $file = rtrim($file, '/');
            } else {
                $file = GeneralUtility::resolveBackPath($file);
            }
        }

        if (!empty($file)) {
            $methodeName = 'addCssFile';

            if ($this->arguments['lib']) {
                $methodeName = 'addCssLibrary';
            }
            $this->pageRenderer->$methodeName(
                $file,
                $this->arguments['rel'],
                $this->arguments['media'],
                $this->arguments['title'],
                $this->arguments['compress'],
                $this->arguments['forceOnTop'],
                $this->arguments['allWrap'],
                $this->arguments['excludeFromConcatenation'],
                $this->arguments['splitChar']
            );
        }

        return '';
    }
}
