<?php
namespace Dia\DiaUtilities\ViewHelpers\Asset;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Page\PageRenderer;

/**
 * Class AbstractAssetViewHelper.
 * Basic ViewHelper class for Asset ViewHelpers.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Witali Rott <witali.rott@die-interaktiven.de>
 * @version 1.0.0
 */
abstract class AbstractAssetViewHelper extends AbstractViewHelper
{
    /**
     * Instance of TYPO3 PageRenderer
     *
     * @var PageRenderer
     */
    protected $pageRenderer;

    /**
     * Inject the TYPO3 PageRenderer
     *
     * @param PageRenderer $pageRenderer
     * @return void
     */
    public function injectPageRenderer(PageRenderer $pageRenderer)
    {
        $this->pageRenderer = $pageRenderer;
    }

    /**
     * Initialize arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('file', 'string', 'File Name', false, NULL);
        $this->registerArgument('name', 'string', 'Block Name', false, NULL);
        $this->registerArgument('compress', 'string|bool', '', false, NULL);
        $this->registerArgument('forceOnTop', 'bool', '', false, FALSE);
        $this->registerArgument('allWrap', 'string', '', false, '');
        $this->registerArgument('excludeFromConcatenation', 'bool', '', false, FALSE);
        $this->registerArgument('splitChar', 'string', '', false, '|');
    }
}
