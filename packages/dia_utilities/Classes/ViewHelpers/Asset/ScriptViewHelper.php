<?php
namespace Dia\DiaUtilities\ViewHelpers\Asset;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Frontend\Page\PageGenerator;


/**
 * Class ScriptViewHelper.
 * For adding JavaScript assets to the page.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @version 1.0.0
 */
class ScriptViewHelper extends AbstractAssetViewHelper
{
    /**
     * Initialize arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('footer', 'bool', 'Adding to Footer', false, FALSE);
        $this->registerArgument('type', 'string', '', false, 'text/javascript');
        $this->registerArgument('async', 'bool', '', false, FALSE);
        $this->registerArgument('integrity', 'string', '', false, '');
    }

    /**
     * Add JavaScript file to the page rendering
     *
     * @return void
     */
    public function render()
    {
        $name = $this->arguments['name'];
        $file = $this->arguments['file'];

        if (empty($file)) {
            $block = $this->renderChildren();
            $name = empty($name) ? md5($block) : $name;
            if (!empty($block)) {
                $file = GeneralUtility::writeJavaScriptContentToTemporaryFile($block);
            }
        } else {
            if (strpos($file, 'EXT:') === 0) {
                $file = GeneralUtility::getFileAbsFileName($file);
                // as the path is now absolute, make it "relative" to the current script to stay compatible
                $file = PathUtility::getRelativePathTo($file);
                $file = rtrim($file, '/');
            } else {
                $file = GeneralUtility::resolveBackPath($file);
            }
        }

        if (!empty($file)) {
            $methodeName = 'addJsFile';

            if ($this->arguments['footer']) {
                $methodeName = 'addJsFooterFile';
            }

            $this->pageRenderer->$methodeName(
                $file,
                $this->arguments['type'],
                $this->arguments['compress'],
                $this->arguments['forceOnTop'],
                $this->arguments['allWrap'],
                $this->arguments['excludeFromConcatenation'],
                $this->arguments['splitChar'],
                $this->arguments['async'],
                $this->arguments['integrity']
            );
        }

        return '';
    }
}
