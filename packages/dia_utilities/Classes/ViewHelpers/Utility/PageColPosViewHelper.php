<?php
namespace Dia\DiaUtilities\ViewHelpers\Utility;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PageColPosViewHelper.
 * ViewHelper to find the colPos value of an element. This might become useful, when using
 * gridelements and you need to find out the real colPos value from a gridelement, wrapped around
 * the content element.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class PageColPosViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('elementId', 'integer', 'The uid of the content element', TRUE, 0);
    }

    /**
     * Recursively steps through containers wrapped around content elements to determine the
     * page colPos, the element is placed inside.
     *
     * @param int $uid The uid of the element to check
     * @return int
     */
    protected function getParentColPos(int $uid): int
    {
        if ($uid === 0) {
            return 0;
        }

        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content');
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll();

        $statement = $queryBuilder->select('*')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
                )
            )
            ->execute();
        while ($record = $statement->fetch()) {
            if ((int)$record['colPos'] < 0 && isset($record['tx_gridelements_container'])) {
                return $this->getParentColPos($record['tx_gridelements_container']);
            } else {
                return $record['colPos'];
            }
        }
    }

    /**
     * Main function of the ViewHelper.
     *
     * @return int
     */
    public function render(): int
    {
        return $this->getParentColPos((int)$this->arguments['elementId']);
    }
}
