<?php
namespace Dia\DiaUtilities\ViewHelpers\Utility\Arrays;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class ExplodeViewHelper.
 * ViewHelper to explode a string into an array and return it.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class ExplodeViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('value', 'string', 'the string to be exploded', TRUE);
        $this->registerArgument('delimiter', 'string', 'The delimiter to be used for exploding, defaults to ","', FALSE);
    } // End of initializeArguments()



    /**
     * Main function of the ViewHelper.
     *
     * @return string
     */
    public function render()
    {
        $delimiter = ',';
        if (!empty($this->arguments['delimiter'])) {
            $delimiter = $this->arguments['delimiter'];
        }
        $as = 'items';
        if (!empty($this->arguments['as'])) {
            $as = $this->arguments['as'];
        }
        $parts = explode($delimiter, $this->arguments['value']);

        $this->templateVariableContainer->add($as, $parts);
        $content = $this->renderChildren();
        $this->templateVariableContainer->remove($as);

        return $content;
        // ====
    } // End of render()
}