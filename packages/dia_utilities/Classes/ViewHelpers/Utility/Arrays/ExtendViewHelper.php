<?php
namespace Dia\DiaUtilities\ViewHelpers\Utility\Arrays;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class ExtendViewHelper.
 * ViewHelper to merge arrays.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class ExtendViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('array', 'array', 'The first array', TRUE, array());
        $this->registerArgument('array2', 'array', 'The second array to be merged to the first one', TRUE, array());
    } // End of initializeArguments()



    /**
     * Main function of the ViewHelper. Merges the given arrays into one and returns the result.
     *
     * @param array		$array		The array to be extended
     * @param array		$array2		The array to extend the first one with
     * @return array				The new array, merged from the given parameters
     */
    public function render($array = array(), $array2 = array())
    {
        if ((count($array) > 0) && (count($array2) > 0)) {
            return array_merge($array, $array2);
            // ====
        }

        return NULL;
        // ====
    } // End of render()
}