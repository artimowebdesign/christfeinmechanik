<?php
namespace Dia\DiaUtilities\ViewHelpers\Utility;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * This ViewHelper may be used to render content multiple times.
 * The content may be given by an argument or use the View Children.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class MultipleViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('steps', 'int', 'The number of loops to perform', TRUE, 0);
        $this->registerArgument('start', 'int', 'Index to start counting from', FALSE, 0);
    } // End of initializeArguments()



    /**
     * Main ViewHelper function. Renders the content.
     *
     * @return string
     */
    public function render()
    {
        $content = '';

        $start = 0;
        if (NULL !== $this->arguments['start']) {
            $start = intval($this->arguments['start']);
        }

        $end = $start + $this->arguments['steps'];

        for ($i = $start; $i < $end; $i++) {
            $content .= $this->renderChildren();
        }

        return $content;
        // ====
    } // End of render()
}