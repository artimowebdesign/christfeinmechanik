<?php
namespace Dia\DiaUtilities\ViewHelpers\Utility;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class MathViewHelper.
 * Provides simple math operations to perform on the given arguments.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class MathViewHelper extends AbstractDiaViewHelper
{
    /**
     * List of short operation names
     *
     * @var array
     */
    protected $operations = [
        '+' => 'add',
        '-' => 'substract',
        '*' => 'multiply',
        '/' => 'divide',
        '%' => 'modulo'
    ];

    /**
     * Initialize Arguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('value1', 'string', 'The first integer for the operation', TRUE, 0);
        $this->registerArgument('value2', 'string', 'The second integer for the operation', TRUE, 1);
        $this->registerArgument('op', 'string', 'The operation to be performed', TRUE, 'add');
        $this->registerArgument('format', 'string', 'The optional result output format', FALSE, 'number');
    } // End of initializeArguments()



    /**
     * Performs a mathematical addition
     *
     * @return string
     */
    protected function add()
    {
        return $this->arguments['value1'] + $this->arguments['value2'];
        // ====
    } // End of add()



    /**
     * Performs a mathematical substration
     *
     * @return string
     */
    protected function substract()
    {
        return $this->arguments['value1'] - $this->arguments['value2'];
        // ====
    } // End of substract()



    /**
     * Performs a mathematical multiplication
     *
     * @return string
     */
    protected function multiply()
    {
        return $this->arguments['value1'] * $this->arguments['value2'];
        // ====
    } // End of multiply()



    /**
     * Performs a mathematical division
     *
     * @return string
     */
    protected function divide()
    {
        return $this->arguments['value1'] / $this->arguments['value2'];
        // ====
    } // End of divide()


    /**
     * Performs modulo operation
     *
     * @return string
     */
    protected function modulo()
    {
        return $this->arguments['value1'] % $this->arguments['value2'];
        // ====
    } // End of modulo()



    /**
     * Performs the mathematical operation and returns the result to the View.
     *
     * @throws \Exception
     * @return mixed  $content       The result of the mathematical operation
     */
    public function render()
    {
        $content = '';

        if (isset($this->arguments['op']) && $this->arguments['op']) {
            $operation = $this->arguments['op'];
            if (isset($this->operations[$operation])) {
                $operation = $this->operations[$operation];
            }

            if (method_exists($this, $operation)) {
                $content = $this->$operation();
            } else {
                throw \Exception('Operation not supported!');
            }
        }

        if (isset($this->arguments['format']) && $this->arguments['format']) {
            switch ($this->arguments['format']) {
                case 'int':
                    $content = (int)$content;
                break;

                case 'float':
                    $content = (float) $content;
                break;

                default:
                break;
            }
        }

        return $content;
        // ====
    } // End of render()
}