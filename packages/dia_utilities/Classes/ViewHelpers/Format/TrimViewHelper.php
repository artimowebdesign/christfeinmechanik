<?php
namespace Dia\DiaUtilities\ViewHelpers\Format;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class TrimViewHelper.
 * ViewHelper to perform php string formatting operations.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @version 1.0.0
 */
class TrimViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('characters', 'string', '', false, '');
    }

    /**
     * Main function of the ViewHelper. Returns the string content of the ViewHelper
     * trimmed by php trim() function.
     *
     * @return string
     */
    public function render()
    {
        $characters = $this->arguments['characters'];
        $content = $this->renderChildren();

        if (false === empty($characters)) {
            $content = trim($content, $characters);
        } else {
            $content = trim($content);
        }

        return $content;
    }
}
