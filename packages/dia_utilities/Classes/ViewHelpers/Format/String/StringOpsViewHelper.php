<?php
namespace Dia\DiaUtilities\ViewHelpers\Format\String;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class StringViewHelper.
 * ViewHelper to perform php string formatting operations.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class StringOpsViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('operations', 'array', 'List of string operations to perform', TRUE);
        $this->registerArgument('string', 'string', 'The string to perform actions on', FALSE);
    } // End of initializeArguments()

    /**
     * Main function of the ViewHelper to render the content.
     *
     * @return string
     */
    public function render()
    {
        if (!is_array($this->arguments['operations'])) {
            return 'Error in format.string ViewHelper!';
            // ====
        }

        // get the source string by argument or tag children
        if (isset($this->arguments['string']) && $this->arguments['string']) {
            $rawString = $this->arguments['string'];
        } else {
            $rawString = $this->renderChildren();
        }

        $content = $rawString;
        foreach ($this->arguments['operations'] as $operation => $arguments) {
            switch ($operation) {
                case 'ucfirst':
                    $content = ucfirst($content);
                break;

                case 'lcfirst':
                    $content = lcfirst($content);
                break;

                case 'strlen':
                    $content = strlen($content);
                break;

                case 'minusToCamelCase':
                    while (($pos = strpos($content, '-')) !== FALSE) {
                        $char = $content[$pos + 1];
                        $content = str_replace('-' . $char, ucfirst($char), $content);
                    }
                break;

                default:
                break;
            }
        }

        return $content;
        // ====
    } // End of render()
}
