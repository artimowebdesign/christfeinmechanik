<?php
namespace Dia\DiaUtilities\ViewHelpers\Format\String;

use Dia\DiaUtilities\ViewHelpers\Abstr\AbstractDiaViewHelper;

/**
 * Class ReplaceViewHelper.
 * ViewHelper perform a string replacement.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.1
 */
class ReplaceViewHelper extends AbstractDiaViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('search', 'string', 'The string to perform replacement in', TRUE);
        $this->registerArgument('replace', 'string', 'The string to perform replacement in', TRUE);
        $this->registerArgument('value', 'string', 'The string to perform replacement in', FALSE);
    } // End of initializeArguments()



    /**
     * The render function, performs the string replacement and returns the result.
     *
     * @return string
     */
    public function render()
    {
        if ($this->arguments['value'] === NULL) {
            $value = $this->renderChildren();
        } else {
            $value = $this->arguments['value'];
        }
        $content = str_replace($this->arguments['search'], $this->arguments['replace'], $value);

        return $content;
        // ====
    } // End of render()
}