<?php
namespace Dia\DiaUtilities\Form\FormDataProvider;

use TYPO3\CMS\Backend\Form\FormDataProviderInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Class PageTsConfig.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities\Form\FormDataProvider
 * @author Witali Rott <witali.rott@die-interaktiven.de>
 * @version 1.0.0
 *
 *
 * Page TsConfig relevant for this record
 *
 * Example:
 *
    TCEFORM.tt_content {
        layout {
            types.header {
                field.header_layout {
                    1 {
                        addItems {
                            red = Red
                        }
                    }
                    2 {
                        addItems {
                            blue = Blue
                        }
                    }
                }
            }
        }
    }
 *
 */
class PageTsConfig implements FormDataProviderInterface
{
    /**
     * Merge type specific page TS to pageTsConfig
     *
     * @param array $result
     * @return array
     */
    public function addData(array $result)
    {
        $mergedTsConfig = $result['pageTsConfig'];

        if (empty($result['pageTsConfig']['TCEFORM.']) || !is_array($result['pageTsConfig']['TCEFORM.'])) {
            $result['pageTsConfig'] = $mergedTsConfig;
            return $result;
        }

        $mergedTsConfig = $result['pageTsConfig'];
        $type = $result['recordTypeValue'];
        $table = $result['tableName'];

        // Merge TCEFORM.[table name].field.[field].[value] over TCEFORM.[table name].[field]
        if (!empty($result['pageTsConfig']['TCEFORM.'][$table . '.'])
            && is_array($result['pageTsConfig']['TCEFORM.'][$table . '.'])
        ) {
            foreach ($result['pageTsConfig']['TCEFORM.'][$table . '.'] as $fieldNameWithDot => $fullFieldConfiguration) {
                $newFieldConfiguration = $fullFieldConfiguration;

                if (!empty($fullFieldConfiguration['types.']) && is_array($fullFieldConfiguration['types.'])) {
                    $typeSpecificConfiguration = $newFieldConfiguration['types.'];
                    if (!empty($typeSpecificConfiguration[$type . '.']) && is_array($typeSpecificConfiguration[$type . '.'])) {
                        $resolvedFieldConfiguration = $this->resolveFieldConfiguration($result, $typeSpecificConfiguration[$type . '.']);
                        unset($newFieldConfiguration['types.'][$type . '.']['field.']);
                        ArrayUtility::mergeRecursiveWithOverrule($newFieldConfiguration['types.'][$type . '.'], $resolvedFieldConfiguration);
                    }
                }
                $mergedTsConfig['TCEFORM.'][$table . '.'][$fieldNameWithDot] = $newFieldConfiguration;
            }
        }

        $result['pageTsConfig'] = $mergedTsConfig;

        return $result;
        // ====
    } // End of addData()



    /**
     * @param $result
     * @param $configuration
     * @return mixed
     */
    private function resolveFieldConfiguration($result, $configuration)
    {
        $databaseRow = $result['databaseRow'];

        if (!empty($configuration['field.']) && is_array($configuration['field.'])) {
            $fieldSpecificConfiguration = $configuration['field.'];
            unset($configuration['field.']);

            foreach($fieldSpecificConfiguration as $fieldValueNameWithDot => $fullValuesSpecificConfiguration) {
                $fieldName = trim($fieldValueNameWithDot, '.');
                if(array_key_exists($fieldName, $databaseRow)) {
                    foreach($fullValuesSpecificConfiguration as $fieldValueWithDot => $fullValuesConfiguration) {
                        $fieldValue = trim($fieldValueWithDot, '.');
                        if((is_array($databaseRow[$fieldName]) && in_array($fieldValue, $databaseRow[$fieldName])) || $databaseRow[$fieldName]==$fieldValue) {

                            ArrayUtility::mergeRecursiveWithOverrule($configuration, $fullValuesConfiguration);
                        }
                    }
                }
            }
        }

        return $configuration;
        // ====
    } // End of resolveFieldConfiguration()
}

