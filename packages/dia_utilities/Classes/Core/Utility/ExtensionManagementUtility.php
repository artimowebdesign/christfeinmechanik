<?php
namespace Dia\DiaUtilities\Core\Utility;

/**
 * Class ExtensionManagementUtility.
 * Extend functionality of TYPO3 ExtensionManagementUtility.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities\Core\Utility
 * @author Witali Rott <witali.rott@die-interaktiven.de>
 * @version 1.0.0
 */
class ExtensionManagementUtility extends \TYPO3\CMS\Core\Utility\ExtensionManagementUtility
{
    /**
     * Remove file from static file include
     *
     * @param $extKey
     * @param string $path
     */
    public static function removeStaticFile($extKey, $path = '')
    {
        if (is_array($GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'])) {
            $items = &$GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'];
            foreach ($items as $key => $item) {
                $extValue = 'EXT:' . $extKey . '/' . $path;
                if (strpos($item[1], $extValue) !== false) {
                    unset($items[$key]);
                }
            }
            sort($items);
        }
    } // End of removeStaticFile()



    /**
     * Remove pageTS config file from tsconfig_includes
     *
     * @param $extKey
     * @param $path
     */
    public static function removePageTSConfigFile($extKey, $path)
    {
        if (is_array($GLOBALS['TCA']['pages']['columns']['tsconfig_includes']['config']['items'])) {
            $items = &$GLOBALS['TCA']['pages']['columns']['tsconfig_includes']['config']['items'];
            foreach ($items as $key => $item) {
                $extValue = 'EXT:' . $extKey . '/' . $path;
                if (strpos($item[1], $extValue) !== false) {
                    unset($items[$key]);
                }
            }
            sort($items);
        }
    } // End of removePageTSConfigFile()
}
