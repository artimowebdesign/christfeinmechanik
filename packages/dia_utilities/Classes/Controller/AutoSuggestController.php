<?php
namespace Dia\DiaUtilities\Controller;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class AutoSuggestController.
 * Utility Controller for Search Requests auto-suggestions.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 1.0.0
 */
class AutoSuggestController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * Ajax action to be called for auto-suggest responses
     *
     * @return string
     */
    public function ajaxAction()
    {
        $term = $_GET['term'];
        $settings = $this->settings['autoSuggest'];
        $fieldList = explode(',', $settings['fieldnames']);
        $constraints = [];
        foreach ($fieldList as $field) {
            $constraints[] = $field . ' LIKE "%' . $term . '%"';
        }

        $query = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($settings['tablename']);
        $limit = $settings['maxItems'];
        $results = $query->select('*')
            ->from($settings['tablename'])
            ->where(implode(' OR ', $constraints))
            ->orderBy('header')
            ->setMaxresults($limit)
            ->execute()
            ->fetchAll();

        foreach ($results as $result) {
            echo $result['header'] . '<br>';
        }

        echo 'Hello';
        exit;
    } // End of ajaxAction()

    /**
     * Display auto-suggest form
     *
     * @return string
     */
    public function showAction()
    {

    } // End of showAction()
}
