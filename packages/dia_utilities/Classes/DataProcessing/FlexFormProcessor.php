<?php
namespace Dia\DiaUtilities\DataProcessing;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class ExtensionManagementUtility.
 * Extend functionality of TYPO3 ExtensionManagementUtility.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @package Dia\DiaUtilities\DataProcessing
 * @author Witali Rott <witali.rott@die-interaktiven.de>
 * @version 1.0.0
 */
class FlexFormProcessor implements DataProcessorInterface
{
    /**
     * @var FlexFormService
     */
    protected $flexFormService;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
    } // End of __construct()



    /**
     * Process field data to split in an array
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData)
    {
        // The field name to process
        $fieldName = $cObj->stdWrapValue('fieldName', $processorConfiguration, 'pi_flexform');
        if (empty($fieldName)) {
            return $processedData;
        }

        $originalValue = $cObj->data[$fieldName];
        if (!is_string($originalValue)) {
            return $processedData;
        }

        // Set the target variable
        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, $fieldName);

        // convert flexform to array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($originalValue);

        $processedData[$targetVariableName] = $flexformData;

        return $processedData;
        // ====
    } // End of process()
}
