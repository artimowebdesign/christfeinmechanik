Extension changelog to TYPO3 extension dia_utilities
====================================================
1.0.1 - 2019-09-02
------------------
* migrating ViewHelpers from EXT dia_base 1.X [MHU]

1.0.0 - 2019-08-13
------------------
* migrating ViewHelpers from dia into this EXT [WRO]

0.2.0 - 2019-07-23
------------------
* adding AutoSuggestController and page type definition for auto-suggest tool for a DB search [MHU]
* migrating some ViewHelpers from dia_base2 into this EXT [MHU]

0.1.0 - 2019-07-15
------------------
* initial extension release as follow-up to dia_fluid_extended [MHU]