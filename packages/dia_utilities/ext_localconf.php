<?php
defined('TYPO3_MODE') or die();

// Configure Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Dia.DiaUtilities',
    'AutoSuggestUtility',
    [
        'AutoSuggest' => 'show, searchMarkersByTerm',
    ],
    // non-cacheable actions
    [
        'AutoSuggest' => '',
    ]
);

// Constants
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
    'dia_utilities',
    'constants',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:dia_utilities/Configuration/TypoScript/Constants.typoscript">',
    'defaultContentRendering'
);

// Setup
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
    'dia_utilities',
    'setup',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:dia_utilities/Configuration/TypoScript/Setup.typoscript">',
    'defaultContentRendering'
);

// Register formEngine
if (TYPO3_MODE === 'BE') {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\Dia\DiaUtilities\Form\FormDataProvider\PageTsConfig::class]['before'][]
        = \TYPO3\CMS\Backend\Form\FormDataProvider\PageTsConfigMerged::class;
}

// Register "dia:" namespace in fluid templates
if (TYPO3_MODE === 'BE') {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['dia'][] = 'Dia\\DiaUtilities\\ViewHelpers';
}
