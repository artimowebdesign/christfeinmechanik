<?php
defined('TYPO3_MODE') or die();

// Register Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Dia.DiaUtilities',
    'AutoSuggestUtility',
    'DIA Utilities'
);
