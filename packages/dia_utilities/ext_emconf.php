<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA Utilities',
    'description' => 'Adding useful utilities to TYPO3.',
    'category' => 'misc',
    'author' => 'Maximilian Hundertmark',
    'author_email' => 'maximilian.hundertmark@die-interaktiven.de',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'date' => '2020-05-29',
    'version' => '1.2.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-10.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
