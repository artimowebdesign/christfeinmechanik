/**
 * JS for the auto-suggest search field utility.
 *
 * @copyright 2019, DIA - die.interaktiven GmbH & Co. KG
 * @author Alexander Bus <alexander.bus@die-interaktiven.de>
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @author Tim Maurice Bayer <tim.bayer@die-interaktiven.de>
 * @version 0.3.0
 */
function SearchAutoSuggest(el, settings={}) {
    const elem = $(el);
    const $suggestionsContainer = document.getElementById("term-suggestions");
    const suggestItems = document.getElementsByClassName('suggestion');


    const ajaxUrl = location.origin + "/?type=4712&action=ajax&term=";

    // suppress browser autocomplete function
    /*
    elem.focus(function(event) {
        this.blur();
        this.focus();
    });
    */


    // prevent submitting search form by hitting enter when autosuggestions is open
    elem.keydown(function(event){
        if( (event.keyCode == 13) && $(this).hasClass('opened') ) {
            event.preventDefault();
            elem.val($('.selected a').data('searchterm'));
            elem.removeClass('opened');
            $($suggestionsContainer).removeClass('items-found');

            return false;
        }
    });

    let $container = $($suggestionsContainer);

    // ajax request for auto complete suggestions, including navigating entries with arrow up/down
    elem.keyup(function (e) {
        if (this.value.length >= 3) {
            switch (e.which) {
                case 40: // arrow down
                    e.preventDefault();
                    let $arrowDown = $('li:not(:last-child).selected');
                    if ($arrowDown.length > 0) {
                        $arrowDown.removeClass('selected')
                            .next().addClass('selected');
                        // console.log($arrowDown.offset().top - $container.offset().top + $container.scrollTop());
                        $container.animate({
                            scrollTop: $arrowDown.offset().top - $container.offset().top + $container.scrollTop()
                        }, 40);
                    }
                    break;

                case 38: // arrow up
                    e.preventDefault();
                    let $arrowUp = $('li:not(:first-child).selected');
                    if ($arrowUp.length > 0) {
                        $arrowUp.removeClass('selected')
                            .prev().addClass('selected');
                        // console.log($arrowUp.offset().top + " - " + $container.offset().top + ' + ' + $container.scrollTop());
                        $container.animate({
                            scrollTop: $arrowUp.offset().top - $container.offset().top + $container.scrollTop() - 80
                        }, 40);
                    }
                    break;

                case 33: // page up
                    e.preventDefault();
                    let $pageUp = $('li:not(:first-child).selected');
                    if ($pageUp.length > 0) {
                        let $prevItems = $pageUp.prevAll();
                        if ($prevItems.length > 1) {
                            if ($prevItems.length >= 5) {
                                $pageUp.removeClass('selected');
                                $prevItems.eq(4).addClass('selected');
                                $container.animate({
                                    scrollTop: $pageUp.offset().top - $container.offset().top + $container.scrollTop() - 320
                                }, 40);
                            } else {
                                $pageUp.removeClass('selected');
                                $prevItems.last().addClass('selected');
                                $container.animate({
                                    scrollTop: $pageUp.offset().top - $container.offset().top + $container.scrollTop() - (40 * ($prevItems.length - 1))
                                }, 40);
                            }
                        } else {
                            if ($prevItems.length > 0) {
                                $pageUp.removeClass('selected').prev().addClass('selected');
                            }
                        }

                    }
                    break;

                case 34: // page down
                    e.preventDefault();
                    let $pageDown = $('li:not(:last-child).selected');
                    if ($pageDown.length > 0) {
                        let $nextItems = $pageDown.nextAll();
                        if ($nextItems.length > 1) {
                            if ($nextItems.length >= 5) {
                                $pageDown.removeClass('selected');
                                $nextItems.eq(4).addClass('selected');
                                $container.animate({
                                    scrollTop: $pageDown.offset().top - $container.offset().top + $container.scrollTop() - (40 * ($('#term-suggestions li').index('.selected') - 1))
                                }, 40);
                            } else {
                                $pageDown.removeClass('selected');
                                $nextItems.last().addClass('selected');
                                $container.animate({
                                    scrollTop: $pageDown.offset().top - $container.offset().top + $container.scrollTop() - (40 * ($('#term-suggestions li').index('.selected') - 1))
                                }, 40);
                            }
                        } else {
                            if ($nextItems.length > 0) {
                                $pageDown.removeClass('selected').next().addClass('selected');
                            }
                        }
                    }
                    break;

                case 13:
                    break;

                default:
                    let xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            $suggestionsContainer.innerHTML = this.responseText;
                            let itemsCount = suggestItems.length;
                            if (itemsCount){
                                $($suggestionsContainer).addClass('items-found');
                                document.getElementById('term').classList.add('opened');
                            }
                            for (let i = 0 ; i < itemsCount; i++) {
                                if (i == 0) {$(suggestItems[0]).parent('li').addClass('selected')}
                                suggestItems.item(i).onclick = function() {
                                    document.getElementById('term').value = this.getAttribute('data-searchterm');
                                    $($suggestionsContainer).removeClass('items-found');
                                    elem.removeClass('opened');
                                    return false;
                                };
                                suggestItems.item(i).onhover = function() {
                                    $('.selected').removeClass('selected');
                                    $($suggestionsContainer[i]).parent('li').addClass("selected");
                                    return false;
                                };
                            }
                        }
                    };

                    let url = ajaxUrl + this.value;
                    xhttp.open("GET", url, true);
                    xhttp.send();
            }
        } else {
            // remove list if input is less than 3
            $($suggestionsContainer).html('');
            $($suggestionsContainer).removeClass('items-found');
            elem.removeClass('opened');
        }
    });
};

SearchAutoSuggest($('#auto-suggest-term'));