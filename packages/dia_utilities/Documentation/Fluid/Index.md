DIA Utilities Fluid ViewHelpers
===============================

Introduction
------------
DIA Utilities provides several Fluid ViewHelpers to extend Fluids capabilities.  
The namespace used is "dia", so you will be able to include the ViewHelpers to any of
your Templates by adding 
```HTML
<dia:[ViewHelperName]>
```
The dia namespace is automatically registered to the Fluid engine by the extensions ext_localconf.php.  
The ViewHelpers are grouped in the type of usage they are made for.

Available ViewHelpers
---------------------
###Common ViewHelpers
Common ViewHelpers are directly included into the dia namespace.
Those ViewHelpers are:  
####SpacelessViewHelper
The SpacelessViewHelper performs a trimming of white-space characters from the content it contains.
White-space characters within HTML tags are not removed.  
The ViewHelper is made for being wrapped around the content, which shall be trimmed and does not
accept nor need any arguments.
#####Example of usage
```HTML
<dia:spaceless>
<div class="container">
    <h1>This is the main header</h1>
    <p>Here goes the bodytext</p>
</div>
</dia:spaceless>
```
The above example will result in the following content rendered:
```HTML
<div class="container"><h1>This is the main header</h1><p>Here goes the bodytext</p></div>
```

Other ViewHelpers are split into categories, so they are stored in subfolders and thus used by adding the according
prefix in the ViewHelper tag.  
Each category has its own documentation section:  
[Asset ViewHelpers](Asset/Index.md)  
ViewHelpers from this category help to integrate assets like StyleSheets or JavaScript into the document rendering.  
[Components ViewHelpers](Components/Index.md)  
ViewHelpers from this category provide HTML components rendering for complexer contents.  
[Data ViewHelpers](Data/Index.md)  
ViewHelpers from this category are providing external data to be used in the Fluid template.  
[Html ViewHelpers](Html/Index.md)  
ViewHelpers from this category generally provide options to directly manipulate the HTML content rendered.  
[Link ViewHelpers](Link/Index.md)  
ViewHelpers from this category enhance the possibilities in link-creation of Fluid.  
[Utility ViewHelpers](Utility/Index.md)  
ViewHelpers from this category generally are useful to fulfill certain tasks, not able to categorize in the other categories.  
[Format ViewHelpers](Format/Index.md)  
ViewHelpers from this category help to format content in certain ways. Basically they are used to format textual contents.

