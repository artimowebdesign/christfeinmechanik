###Components ViewHelpers
Components ViewHelpers provide rendering of custom HTML components.  

####CategoryFilterViewHelper
The CategoryFilterViewHelper fetches sys_category records, having relations to a given set of other records.  
This allows to create filter elements, only displaying relevant categories, according to the set of records.  
You may provide sys_collection records to fetch all categories, related to files being part of the collections, or
provide a list of record uids and the tablename of the DB table where the records are stored.  
Optionally you may pass a parent category uid to only recognize categories having this parent.

**Arguments**  
None of the arguments is marked "required", but you have to provide eather the collections parameter or the
items AND table arguments, to make it work!
<table>
    <tr>
        <th>Argument name</th><th>Type</th><th>required?</th><th>default value</th><th>usage</th>
    </tr>
    <tr>
        <td>as</td><td>string</td><td>no</td><td>items</td><td>overrides the variable name, where content is put into the View</td>
    </tr>
    <tr>
        <td>collections</td><td>string</td><td>no</td><td></td><td>comma-separated list of sys_collection records to display the list content</td>
    </tr>
    <tr>
        <td>items</td><td>array</td><td>no</td><td></td><td>List of items to create the filter for</td>
    </tr>
    <tr>
        <td>table</td><td>string</td><td>no</td><td></td><td>tablename, where records are stored</td>
    </tr>
    <tr>
        <td>parent</td><td>int</td><td>no</td><td></td><td>uid of a parent category, each filter category should have</td>
    </tr>
</table>

**Child Content**  
The ViewHelper is to be used, wrapping the content, where the items are needed. It adds a variable, named "items"
(or else, if you override the name by passing the argument "as"), containing all sys_category items, having a 
relation to the given set of records.

#####Example of usage
```HTML
<!-- assuming, that data.records contains a list of tt_content items, just like field "records" from tt_content:shortcut -->
<dia:components.categoryFilter items="{data.records}" table="tt_content">
    <select>
    <!-- {items} is provided by dia:components.categoryFilter -->
    <f:for each="{items}" as="category">
        <option value="{category.uid}">{category.title}</option>
    </f:for>
    </select>
</f:for>
</dia:components.categoryFilter>
```