###Format ViewHelpers
These ViewHelpers provide possibilites to format content in certain ways.  
ViewHelpers of this category are split up into sub-categories, determining, which content is especially targeted
by each ViewHelper.  
Current list of ViewHelpers in this category:  
- [TrimViewHelper](#TrimViewHelper)
- [String.ReplaceViewHelper](#ReplaceViewHelper)
- [String.StringOpsViewHelper](#StringOpsViewHelper)


####TrimViewHelper
The TrimViewHelper actually applies php's trim() function on the string content provided.  
You may provide an optional character list, which shall be trimmed:
| Argument name | Type   | required | default |  
| ------------- | ------ | -------- | ------- |  
| characters    | string | false    | php trim() default value |
#####Example of usage
```HTML
<dia:trim characters="+ -">
    <p>+ My text has some unwanted characters----</p>
</dia:trim>
```
The above example will result in the following content rendered:
```HTML
<p>My text has some unwanted characters</p>
```

###Format.String ViewHelpers
These ViewHelpers are especially used to perform String operations.
####ReplaceViewHelper
This ViewHelper performs the php str_replace function on a given string.  
The string to perform the replacement on, might be passed as argument "value" or by wrapping the ViewHelper 
around content to perform the replacement in.    
| Argument name | Type   | required | default |
| ------------- | ------ | -------- | ------- |
| search        | string | true     |         |
| replace       | string | true     |         |
| value         | string | false    |         |
#####Example of usage
```HTML
<dia:format.string.replace search="," replace=".">
Hier ist eine Zahl: 10,34.
</dia:format.string.replace>

<dia.format.string.replace search="," replace="." value="13,5" />
```

The above example will result in:
```HTML
Hier ist eine Zahl: 10.34.

13.5
```
####StringOpsViewHelper
This ViewHelper provides a small number of utility functions to be applied on string content, 
reguarly based on functions php provides.  
Currently, the ViewHelper provides the following operations:  
- transform the first character to uppercse (php: ucfirst())
- transform the first character to lowercase (php: lcfirst())
- get the length of the string as integer value (php: strlen())
- transform '-' characters into CamelCase writing  
You determine the operation to be performed, by passing the argument 'operations'. This
argument has to be an array and allows to perform more than one operation in a row.  
The ViewHelper may work with the content inside its tag or may be passed a second argument,
named 'string' to provide the string content to be worked on.

| Argument name | Type | required | default |  
| operations | array | true | |  
| string | string | false | |
#####Example of usage
```HTML
<!-- let {string} contain the value: 'my text-content might look a little nicer...' -->
<p>
<dia:string.stringOps operations="{'minusToCamelCase', 'ucfirst'}">
{string}
</dia:string.stringOps>
and has a length of: <dia.string.stringOps operations="'strlen'" string="{string}"/> characters
</p>
```
The above example will result in the following HTML content:
```HTML
<p>My textContent might look a little nicer... and has a length of 43 characters</p>
```