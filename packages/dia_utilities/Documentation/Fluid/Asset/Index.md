###Asset ViewHelpers
Asset ViewHelpers help to integrate assets like StyleSheets or JavaScript into the document rendering.

####ScriptViewHelper
This ViewHelpers allows to add JavaScript file inclusions into the rendering process.  
You are able to use all options, TypoScript provides for including JavaScript files.

**Arguments**  
<table>
    <tr>
        <th>Argument name</th><th>Type</th><th>required?</th><th>default value</th><th>usage</th>
    </tr>
    <tr>
        <td>file</td><td>string</td><td>yes</td><td></td><td>Path to the JavaScript file to include</td>
    </tr>
    <tr>
        <td>name</td><td>string</td><td>?</td><td>?</td><td>?</td>
    </tr>
    <tr>
        <td>compress</td><td>boolean</td><td>no</td><td>TRUE</td><td>If set to FALSE, the file will be excluded from JavaScript file compression</td>
    </tr>
    <tr>
        <td>forceOnTop</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the file inclusion will be made BEFORE any other files included in the section</td>
    </tr>
    <tr>
        <td>allWrap</td><td>string</td><td>no</td><td></td><td>Defines a wrap to wrap around the script-tag, useful for additional comments or so</td>
    </tr>
    <tr>
        <td>excludeFromConcatenation</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the file will be excluded from js concatenation</td>
    </tr>
    <tr>
        <td>splitChar</td><td>string</td><td>no</td><td>|</td><td>Overrides the default TypoScript StdWrap split character if set</td>
    </tr>
    <tr>
        <td>footer</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the JavaScript file while be included before the closing body-Tag</td>
    </tr>
    <tr>
        <td>type</td><td>string</td><td>no</td><td>text/javascript</td><td>Overrides the type-Attribute of the script-tag if set</td>
    </tr>
    <tr>
        <td>async</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the script-tag will have set the async-property to make the file load asynchronously</td>
    </tr>
    <tr>
        <td>integrity</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the integrity-attribute will be added to the script tag</td>
    </tr>
</table>

**Example of usage**  
```HTML

``` 

####StyleViewHelper
This ViewHelpers allows to add JavaScript file inclusions into the rendering process.  
You are able to use all options, TypoScript provides for including JavaScript files.

**Arguments**  
<table>
    <tr>
        <th>Argument name</th><th>Type</th><th>required?</th><th>default value</th><th>usage</th>
    </tr>
    <tr>
        <td>file</td><td>string</td><td>yes</td><td></td><td>Path to the CSS file to include</td>
    </tr>
    <tr>
        <td>name</td><td>string</td><td>?</td><td>?</td><td>?</td>
    </tr>
    <tr>
        <td>compress</td><td>boolean</td><td>no</td><td>TRUE</td><td>If set to FALSE, the file will be excluded from CSS file compression</td>
    </tr>
    <tr>
        <td>forceOnTop</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the file inclusion will be made BEFORE any other files included in the section</td>
    </tr>
    <tr>
        <td>allWrap</td><td>string</td><td>no</td><td></td><td>Defines a wrap to wrap around the style-tag, useful for additional comments or so</td>
    </tr>
    <tr>
        <td>excludeFromConcatenation</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the file will be excluded from css concatenation</td>
    </tr>
    <tr>
        <td>splitChar</td><td>string</td><td>no</td><td>|</td><td>Overrides the default TypoScript StdWrap split character if set</td>
    </tr>
    <tr>
        <td>rel</td><td>string</td><td>no</td><td>stylesheet</td><td>Overrides the rel-attribute, if set</td>
    </tr>
    <tr>
        <td>media</td><td>string</td><td>no</td><td>all</td><td>Overrides the media-Attribute of the script-tag if set</td>
    </tr>
    <tr>
        <td>title</td><td>string</td><td>no</td><td></td><td>If set to TRUE, the script-tag will have set the async-property to make the file load asynchronously</td>
    </tr>
    <tr>
        <td>lib</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the file is included in the includeCSSLibs section</td>
    </tr>
    <tr>
        <td>inline</td><td>boolean</td><td>no</td><td>FALSE</td><td>If set to TRUE, the styles are included as inline style-tag</td>
    </tr>
</table>

**Example of usage**  
```HTML

``` 