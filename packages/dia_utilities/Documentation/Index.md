TYPO3 Extension Documentation dia-utilities
===========================================

History
-------
This extension is a follow-up to the dia_fluid_extended Extension.  
Also, some Features of some other DIA Extensions are merged into this Extension.

What does it do?
----------------
dia-utilities provides several utilities to make TYPO3 implementation easier.  
Those are Fluid ViewHelpers, Typocript libraries and a small Plugin, providing a search input field
with auto-suggest function.

Table of contents
-----------------
TypoScript [TypoScript libraries] ./TypoScript/Index.md
Fluid ViewHelpers
Auto-Suggest Form