<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA GE',
    'description' => 'DIA GE based on Bootstrap',
    'category' => 'plugin',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '10.0.2',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-10.99.99',
            'gridelements' => '9.0.0-10.99.99'
        ],
        'conflicts' => [],
        'suggests' => [
            'dia' => '',
        ],
    ],
];
