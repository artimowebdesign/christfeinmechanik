<?php
defined('TYPO3_MODE') or die();

// Override GridElements DrawItem Hook
call_user_func(function ($_EXTKEY) {
        $gridelementsHook = \GridElementsTeam\Gridelements\Hooks\DrawItem::class;
        if (in_array($gridelementsHook, $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'])) {
            $gridelementsHookKey = array_search($gridelementsHook, $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']);
            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$gridelementsHookKey] = \Dia\DiaGe\Hooks\DrawItem::class;
        }
}, $_EXTKEY);

// Add recStatInfo Hook
call_user_func(function ($_EXTKEY) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['recStatInfoHooks'][] = \Dia\DiaGe\Hooks\DrawItem::class . '->recStatInfoHooks';
}, $_EXTKEY);
