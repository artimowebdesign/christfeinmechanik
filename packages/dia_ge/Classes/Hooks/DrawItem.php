<?php

namespace Dia\DiaGe\Hooks;

use GridElementsTeam\Gridelements\Backend\LayoutSetup;
use GridElementsTeam\Gridelements\Helper\Helper;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Database\QueryGenerator;
use TYPO3\CMS\Core\Database\ReferenceIndex;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * DrawItem
 */
class DrawItem extends \GridElementsTeam\Gridelements\Hooks\DrawItem implements PageLayoutViewDrawItemHookInterface
{


    /**
     * Processes the item to be rendered before the actual example content gets rendered
     * Deactivates the original example content output
     *
     * @param PageLayoutView $parentObject : The parent object that triggered this hook
     * @param boolean $drawItem : A switch to tell the parent object, if the item still must be drawn
     * @param string $headerContent : The content of the item header
     * @param string $itemContent : The content of the item itself
     * @param array $row : The current data row for this item
     *
     * @return void
     */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        parent::preProcess($parentObject, $drawItem, $headerContent, $itemContent, $row);

        $headerContent = '';
    }

    /**
     * Renders the grid layout table after the HTML content for the single elements has been rendered
     *
     * @param array $layoutSetup : The setup of the layout that is selected for the grid we are going to render
     * @param array $row : The current data row for the container item
     * @param array $head : The data for the column headers of the grid we are going to render
     * @param array $gridContent : The content data of the grid we are going to render
     *
     * @return string
     */
    public function renderGridLayoutTable($layout, $row, $head, $gridContent, PageLayoutView $parentObject)
    {
        $grid = parent::renderGridLayoutTable($layout, $row, $head, $gridContent, $parentObject);
        $grid = '<div class="t3-grid-container-wrapper t3-grid-type-' . $row['tx_gridelements_backend_layout'] . '">' . $grid . '</div>';

        return $grid;
    }

    /**
     * renders the HTML output for elements of the CType gridelements_pi1
     * @param array $_params : The current data row for this item
     * @param PageLayoutView $parentObject : The parent object that triggered this hook
     */
    public function recStatInfoHooks($_params, $parentObject = NULL)
    {
        $row = $_params[2];
        if ($row['header']) {
            $infoArr = array();
            $parentObject->getProcessedValue('tt_content', 'header_position,header_layout,header_link', $row, $infoArr);
            $hiddenHeaderNote = '';
            // If header layout is set to 'hidden', display an accordant note:
            if ($row['header_layout'] == 100) {
                $hiddenHeaderNote = ' <em>[' . LocalizationUtility::translate('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.hidden') . ']</em>';
            }
            $outHeader = '<strong class="dia_ge-title">' . $parentObject->linkEditContent($parentObject->renderText($row['header']), $row) . $hiddenHeaderNote . '</strong>';
            $outHeader .= $row['date'] ? '<span class="dia_ge-date">' . htmlspecialchars($parentObject->itemLabels['date'] . ': ' . BackendUtility::date($row['date'])) . '</span>' : '';
        }
        return $outHeader;
    }
}
