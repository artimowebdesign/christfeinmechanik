<?php

namespace Dia\DiaGe\Plugin;

/**
 * Class Gridelements
 * @package Dia\DiaGe\Plugin
 */
class Gridelements extends \GridElementsTeam\Gridelements\Plugin\Gridelements
{
    /**
     * fetches values from the grid flexform and assigns them to virtual fields in the data array
     * @param array $child
     */
    public function getPluginFlexFormData(&$child = [])
    {
        if (!empty($child)) {
            $cObjData = $child;
        } else {
            $cObjData = $this->cObj->data;
        }

        $pluginFlexForm = $cObjData['pi_flexform'];

        if (is_array($pluginFlexForm) && is_array($pluginFlexForm['data'])) {
            foreach ($pluginFlexForm['data'] as $sheet => $data) {
                if (is_array($data)) {
                    foreach ((array)$data as $language => $value) {
                        if (is_array($value)) {
                            foreach ((array)$value as $key => $val) {
                                $cObjData['flexform_' . $sheet . '_' . $key] = $this->getFlexFormValue(
                                    $pluginFlexForm,
                                    $key,
                                    $sheet
                                );
                            }
                        }
                    }
                }
            }
        }

        unset($pluginFlexForm);

        if (!empty($child)) {
            $child = $cObjData;
        } else {
            $this->cObj->data = $cObjData;
        }

        unset($cObjData);
    }

    /**
     * renders the columns of the grid container and returns the actual content
     *
     * @param array $setup The adjusted setup of the grid container
     *
     * @return array $content The raw HTML output of the grid container before stdWrap functions will be applied to it
     */
    public function renderColumnsIntoParentGrid($setup = array())
    {
        $content = parent::renderColumnsIntoParentGrid($setup);

        $rawColumnsData = array();

        if (is_array($this->cObj->data['tx_gridelements_view_raw_columns'])) {
            foreach ($this->cObj->data['tx_gridelements_view_raw_columns'] as $column => $rawColumns) {
                foreach ($rawColumns as $rawData) {
                    if (isset($rawData['uid']) && isset($this->cObj->data['tx_gridelements_view_child_' . $rawData['uid']])) {
                        $rawColumnsData[$column][] = array(
                            'raw' => $rawData,
                            'content' => $this->cObj->data['tx_gridelements_view_child_' . $rawData['uid']]
                        );
                    }
                }
            }
        }
        $this->cObj->data['tx_gridelements_columns_data'] = $rawColumnsData;

        return $content;
    }
}
