<?php

namespace Dia\DiaGe\Service;

/**
 * FlexForm.
 * Servie class for FlexForm configuration.
 *
 * @copyright 2018, DIA - die.interaktiven GmbH & Co. KG
 * @author Witali Rott <witali.rott@die-interaktiven.de>
 * @author Maximilian Hundertmark <maximilian.hundertmark@die-interaktiven.de>
 * @version 8.1.5
 */
class FlexForm
{
    /**
     * Returns an array of dropdown options for selection of column widths.
     *
     * @param array $config
     * @return array
     */
    public function getColumnOptions($config)
    {
        $items = array();
        if (!empty($config['config']['gridColumns'])) {
            for ($i = 0; $i <= $config['config']['gridColumns']; ++$i)
                $items[] = array($i, $i);

            $config['config']['default'] = 12;

            $config['row'][$config['field']] = 12;
        }
        $config['items'] = $items;
//        print_r($config);
        return $config;
        // ====
    } // End of getColumnOptions()


    /**
     * Returns an array of dropdown options for selection of column offsets.
     *
     * @param array $config
     * @return array
     */
    public function getOffsetOptions($config)
    {
        $items = array();

        if (!empty($config['config']['gridColumns'])) {
            for ($i = 0; $i < ($config['config']['gridColumns'] / 2); $i++) {
                $items[] = array($i, $i);
            }
            $config['items'] = $items;

            $config['config']['default'] = 0;

            $config['row'][$config['field']] = 12;
        }

        return $config;
        // ====
    } // End of getOffsetOptions()
}