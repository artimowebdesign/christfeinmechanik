<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'dia_ge';

call_user_func(function ($_EXTKEY, $_TABLE) {
    if (is_array($GLOBALS['TCA'][$_TABLE]['columns']['tx_gridelements_children']['config']['appearance'])) {
        $GLOBALS['TCA'][$_TABLE]['columns']['tx_gridelements_children']['config']['appearance']['useSortable'] = true;
        $GLOBALS['TCA'][$_TABLE]['columns']['tx_gridelements_children']['config']['appearance']['enabledControls']['dragdrop'] = true;
        $GLOBALS['TCA'][$_TABLE]['columns']['tx_gridelements_children']['config']['appearance']['enabledControls']['sort'] = true;
    }
}, $_EXTKEY, 'tt_content');
