<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'dia_ge';

call_user_func(function ($_EXTKEY) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'DIA GE');
}, $_EXTKEY);
