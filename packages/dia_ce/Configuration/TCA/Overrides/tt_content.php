<?php
defined('TYPO3_MODE') or die();

/**
 * Header Date
 */
call_user_func(function($_EXTKEY, $_TABLE) {
    $GLOBALS['TCA'][$_TABLE]['columns']['date']['config']['eval'] = 'datetime,int';
}, 'dia_ce', 'tt_content');

/**
 * Header Style
 */
call_user_func(function($_EXTKEY, $_TABLE) {
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';
    $languageGeneralFilePrefix = 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:';
    $languageCoreFilePrefix = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:';
    $languageFrontendFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    $GLOBALS['TCA'][$_TABLE]['columns']['header_position']['config']['items'] = [
        [$languageGeneralFilePrefix . 'LGL.default_value', NULL],
        [$languageFrontendFilePrefix . 'header_position.I.3', 'left', 'fa-align-left'],
        [$languageFrontendFilePrefix . 'header_position.I.1', 'center', 'fa-align-center'],
        [$languageFrontendFilePrefix . 'header_position.I.2', 'right', 'fa-align-right'],
    ];

    $TCAcolumns = [];
    $TCAcolumns['header_style'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.header_style',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => 0,
            'items' => [
                [$languageGeneralFilePrefix . 'LGL.default_value', 0],
                [$languageFrontendFilePrefix . 'header_layout.I.1', 1],
                [$languageFrontendFilePrefix . 'header_layout.I.2', 2],
                [$languageFrontendFilePrefix . 'header_layout.I.3', 3],
                [$languageFrontendFilePrefix . 'header_layout.I.4', 4],
                [$languageFrontendFilePrefix . 'header_layout.I.5', 5],
            ],
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'header',
        'header_style',
        'after:header_layout'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'headers',
        'header_style',
        'after:header_layout'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'buttonHeaders',
        'header_style',
        'after:header_layout'
    );
}, 'dia_ce', 'tt_content');

/**
 * Header Color
 */
call_user_func(function($_EXTKEY, $_TABLE) {
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';
    $languageGeneralFilePrefix = 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:';

    $TCAcolumns = [];
    $TCAcolumns['header_color'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.header_color',
        'config' => [
            'fieldWizard' => [
                'selectIcons' => [
                    'disabled' => FALSE,
                ],
            ],
            'type' => 'select',
            'renderType' => 'selectSingle',
            'maxitems' => 1,
            'size' => 1,
            'default' => 0,
            'items' => [
                [$languageGeneralFilePrefix . 'LGL.default_value', 0],
            ]
        ]
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'header',
        'header_color, --linebreak--',
        'before:header_link'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'headers',
        'header_color, --linebreak--',
        'before:header_link'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'buttonHeaders',
        'header_color, --linebreak--',
        'before:header_link'
    );
}, 'dia_ce', 'tt_content');

/**
 * Classes
 */
call_user_func(function($_EXTKEY, $_TABLE) {
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';

    $TCAcolumns = [];
    $TCAcolumns['tx_diacontent_classes'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.tx_diacontent_classes',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectCheckBox',
            'maxitems' => 999,
            'enableMultiSelectFilterTextfield' => TRUE,
            'items' => []
        ]
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'tx_diacontent_classes', '', 'after:space_after_class');
}, 'dia_ce', 'tt_content');

/**
 * Space Classes
 */
call_user_func(function($_EXTKEY, $_TABLE) {
    $languageLocalFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    $spaceItems = [
        [$languageLocalFilePrefix . 'space_class_default', ''],
        [$languageLocalFilePrefix . 'space_class_none', 'none'],
        [$languageLocalFilePrefix . 'space_class_small', 'small'],
        [$languageLocalFilePrefix . 'space_class_medium', 'medium'],
        [$languageLocalFilePrefix . 'space_class_large', 'large'],
    ];

    $TCAcolumns = [];
    $TCAcolumns['space_inner_before_class'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.space_inner_before_class',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => '',
            'items' => $spaceItems
        ]
    ];
    $TCAcolumns['space_inner_after_class'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.space_inner_after_class',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => '',
            'items' => $spaceItems
        ]
    ];
    $TCAcolumns['height_class'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.height_class',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'default' => '',
            'items' => [
                [$languageLocalFilePrefix . 'height_class.default', ''],
                [$languageLocalFilePrefix . 'height_class.25', '25'],
                [$languageLocalFilePrefix . 'height_class.50', '50'],
                [$languageLocalFilePrefix . 'height_class.75', '75'],
                [$languageLocalFilePrefix . 'height_class.100', '100'],
            ]
        ]
    ];
    $GLOBALS['TCA'][$_TABLE]['columns']['space_before_class']['config']['items'] = $spaceItems;
    $GLOBALS['TCA'][$_TABLE]['columns']['space_after_class']['config']['items'] = $spaceItems;

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'frames',
        'height_class;' . $languageLocalFilePrefix . 'height_class_formlabel,--linebreak--,',
        'after:layout'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'frames',
        'space_inner_before_class;' . $languageLocalFilePrefix . 'space_inner_before_class_formlabel, space_inner_after_class;' . $languageLocalFilePrefix . 'space_inner_after_class_formlabel,',
        'after:space_after_class'
    );
}, 'dia_ce', 'tt_content');
