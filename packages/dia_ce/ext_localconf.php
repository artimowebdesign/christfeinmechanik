<?php
defined('TYPO3_MODE') or die();

// Adding TsConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TsConfig/Page.tsconfig">'
);

// translate the default tt_content language xlf file
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_ttc.xlf']['dia_ce']
    = 'EXT:dia_ce/Resources/Private/Language/locallang_ttc.xlf';
