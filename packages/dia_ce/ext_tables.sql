#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	tx_diacontent_classes text,
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	space_inner_before_class varchar(60) DEFAULT '' NOT NULL,
	space_inner_after_class varchar(60) DEFAULT '' NOT NULL,
	height_class varchar(60) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	header_style tinytext,
	header_position tinytext,
	header_color tinytext
);
