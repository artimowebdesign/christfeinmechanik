### Define Classes

####Page-TSconfig
for all tt_content elements

``` typoscript
TCEFORM.tt_content.tx_diacontent_classes {
  addItems {
    class1 = Class 1
    class2 = Class 2
    multiple\.classes = Multiple Classes
  }
}
```
for a specific CType
```
TCEFORM.tt_content.tx_diacontent_classes.types.[CType] {
  addItems {
    h1 = H 1
    h2 = H 2
    bold\.h3 = Bold H3
  }
}
```