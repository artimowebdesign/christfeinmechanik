<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA CE',
    'description' => 'DIA Content Elements Base Extension',
    'category' => 'fe',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '10.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.99.99',
            'dia_utilities' => '1.0.0-1.99.99'
        ],
        'conflicts' => [
            'dia_content' => ''
        ],
        'suggests' => [],
    ],
];
