<?php
defined('TYPO3_MODE') or die();

// Register as a skin
$GLOBALS['TBE_STYLES']['skins']['dia_ce'] = [
    'name' => 'dia_ce',
    'stylesheetDirectories' => [
        'css' => 'EXT:dia_ce/Resources/Public/Backend/Css/'
    ]
];

if (TYPO3_MODE=="BE")
{
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Imaging\IconRegistry::class
    );

    // FontAwesome Icons
    $fontAwesomeIcons = [
        'align-left',
        'align-center',
        'align-right',
        'align-justify',
    ];
    foreach($fontAwesomeIcons as $fontAwesomeIcon)
    {
        $iconRegistry->registerIcon(
            'fa-' . $fontAwesomeIcon,
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => $fontAwesomeIcon]
        );
    }
}
