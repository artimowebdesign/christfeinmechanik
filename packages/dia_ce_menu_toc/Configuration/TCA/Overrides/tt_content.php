<?php
defined('TYPO3_MODE') or die();

/**
 * Add Content Element
 */
call_user_func(function ($_EXTKEY, $_TABLE, $CType = 'dia_menu_toc', $CTypeIcon = 'content-menu-pages') {
    /**
     * Language Files
     */
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';
    $languageCoreFilePrefix = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:';
    $languageFrontendFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    /**
     * Add content element to selector list
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        $_TABLE,
        'CType',
        [
            $languageLocalFilePrefix . 'tt_content.' . $CType . '.title',
            $CType,
            $CTypeIcon
        ],
        'menu_sitemap_pages',
        'after'
    );

    /**
     * Assign Icon
     */
    $GLOBALS['TCA'][$_TABLE]['ctrl']['typeicon_classes'][$CType] = $CTypeIcon;

    /**
     * Configure element type
     */
    $GLOBALS['TCA'][$_TABLE]['types'][$CType] = $GLOBALS['TCA'][$_TABLE]['types']['menu_section'];

}, 'dia_ce_menu_toc', 'tt_content');
