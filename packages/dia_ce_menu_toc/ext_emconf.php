<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA CE Menu Toc',
    'description' => 'DIA CE Menu Table of Contents Element',
    'category' => 'frontend',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '10.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.99.99',
            'dia_ce' => '9.0.0-10.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
