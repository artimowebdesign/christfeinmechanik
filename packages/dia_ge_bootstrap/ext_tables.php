<?php
defined('TYPO3_MODE') or die();

// Add TBE style
call_user_func(function ($_EXTKEY) {
    if (TYPO3_MODE == "BE") {
        $GLOBALS['TBE_STYLES']['skins'][$_EXTKEY] = array(
            'name' => $_EXTKEY,
            'stylesheetDirectories' => array(
                'css' => 'EXT:dia_ge_bootstrap/Resources/Public/Backend/Css/'
            )
        );
    }
}, $_EXTKEY);
