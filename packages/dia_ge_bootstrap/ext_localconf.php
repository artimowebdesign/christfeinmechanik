<?php
defined('TYPO3_MODE') or die();

// Update Wizards
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['DiaDiaGeBootstrapUpdatesGridelementsContentElementUpdate']
    = \Dia\DiaGeBootstrap\Updates\GridelementsContentElementUpdate::class;
