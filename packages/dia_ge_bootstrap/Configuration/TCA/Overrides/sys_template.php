<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'dia_ge_bootstrap';

call_user_func(function ($_EXTKEY) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'DIA GE Bootstrap');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Compatibility/Bootstrap3', 'DIA GE Bootstrap 3 Compatibility');

    // remove static files for dia_ge and gridelements
    \Dia\DiaUtilities\Core\Utility\ExtensionManagementUtility::removeStaticFile('dia_ge');
    \Dia\DiaUtilities\Core\Utility\ExtensionManagementUtility::removeStaticFile('gridelements');
}, $_EXTKEY);
