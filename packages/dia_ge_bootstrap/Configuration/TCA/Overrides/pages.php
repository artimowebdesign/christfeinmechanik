<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'dia_ge_bootstrap';

$elementIdentifiers = [
    'Container',
    'Column',
    '2Columns',
    '3Columns',
    '4Columns',
    '5Columns',
    '6Columns',
    'Accordion',
    'Slider',
    'ImageGallery',
    'Tabs',
    'Timeline',
];

call_user_func(function ($_EXTKEY, $elementIdentifiers) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        $_EXTKEY,
        'Configuration/TsConfig/Page/ContentElement/AllGrid.tsconfig',
        'DIA Gridelements Bootstrap All Grid Content Elements'
    );

    foreach ($elementIdentifiers as $elementIdentifier) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $_EXTKEY,
            'Configuration/TsConfig/Page/ContentElement/Element/' . $elementIdentifier . '.tsconfig',
            'DIA Gridelements Bootstrap Content Element: ' . $elementIdentifier
        );
    }
}, $_EXTKEY, $elementIdentifiers);

