lib.gridelements.bootstrapGridSetup {
    # Bootstrap Column
    Column.columns < lib.gridelements.defaultGridSetup.columns
    Column.cObject =< lib.gridelements.defaultGridSetup.cObject
    Column.cObject.variables {
        rowClasses = COA
        rowClasses {
            10 = TEXT
            10.value = row

            20 = TEXT
            20.field = flexform_advanced_justifyContent
            20.noTrimWrap = | ||
            20.if.isTrue.field = flexform_advanced_justifyContent

            30 = TEXT
            30 {
                field = flexform_advanced_alignItems
                noTrimWrap = | ||
                if.isTrue.field = flexform_advanced_alignItems
            }

            40 = TEXT
            40 {
                value = no-gutters
                noTrimWrap = | ||
                if.isTrue.field = flexform_advanced_noGutters
            }
        }

        colOneClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colOneClasses.10.key = one

        colTwoClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colTwoClasses.10.key = two

        colThreeClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colThreeClasses.10.key = three

        colFourClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colFourClasses.10.key = four

        colFiveClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colFiveClasses.10.key = five

        colSixClasses =< styles.tx_diagebootstrap.flexToBootstrap
        colSixClasses.10.key = six
    }

    # Bootstrap 2Columns
    2Columns.columns < lib.gridelements.defaultGridSetup.columns
    2Columns.cObject =< lib.gridelements.bootstrapGridSetup.Column.cObject

    # Bootstrap 3Columns
    3Columns.columns < lib.gridelements.defaultGridSetup.columns
    3Columns.cObject =< lib.gridelements.bootstrapGridSetup.Column.cObject

    # Bootstrap 4Columns
    4Columns.columns < lib.gridelements.defaultGridSetup.columns
    4Columns.cObject =< lib.gridelements.bootstrapGridSetup.Column.cObject

    # Bootstrap 5Columns
    5Columns.columns < lib.gridelements.defaultGridSetup.columns
    5Columns.cObject =< lib.gridelements.bootstrapGridSetup.Column.cObject
    5Columns.cObject.variables {
        rowClasses.10.value = row-15
    }

    # Bootstrap 6Columns
    6Columns.columns < lib.gridelements.defaultGridSetup.columns
    6Columns.cObject =< lib.gridelements.bootstrapGridSetup.Column.cObject

    # Bootstrap Accordion
    Accordion.columns < lib.gridelements.defaultGridSetup.columns
    Accordion.cObject =< lib.gridelements.defaultGridSetup.cObject

    # Bootstrap Container
    Container.columns < lib.gridelements.defaultGridSetup.columns
    Container.cObject =< lib.gridelements.defaultGridSetup.cObject
    Container.cObject.variables {
        containerType = TEXT
        containerType {
            field = flexform_general_containerType
            ifNull = container
        }
        frameClasses = COA
        frameClasses {
            100 = TEXT
            100.value = frame-gridelements-bg-image
            100.noTrimWrap = | ||
            100.if.isTrue.field = flexform_background_backgroundImage

            200 = TEXT
            200.value = frame-gridelements-bg-color
            200.noTrimWrap = | ||
            200.if.isTrue.field = flexform_background_backgroundColor
        }

        // Fallback to v8
        elementClasses < .frameClasses
        elementClasses {
            100.value = bg-image
            200.value = bg-color
        }
    }

    # Bootstrap ImageGallery
    ImageGallery.columns < lib.gridelements.defaultGridSetup.columns
    ImageGallery.cObject =< lib.gridelements.defaultGridSetup.cObject
    ImageGallery.cObject {
        variables {
            colClasses = COA
            colClasses {
                10 = TEXT
                10 {
                    value = 12 / {field:flexform_general_numColumns}
                    value.insertData = 1
                    prioriCalc = intval
                    noTrimWrap = | col-md-||
                }
            }
        }

        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                collections.field = flexform_general_collection
                as = files
            }
        }
    }

    # Bootstrap Slider
    Slider.columns < lib.gridelements.defaultGridSetup.columns
    Slider.cObject =< lib.gridelements.defaultGridSetup.cObject

    # Bootstrap Tabs
    Tabs.columns < lib.gridelements.defaultGridSetup.columns
    Tabs.cObject =< lib.gridelements.defaultGridSetup.cObject

    # Bootstrap Timeline
    Timeline.columns < lib.gridelements.defaultGridSetup.columns
    Timeline.cObject =< lib.gridelements.defaultGridSetup.cObject
}
