<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA GE Bootstrap',
    'description' => 'DIA GE Bootstrap',
    'category' => 'plugin',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '10.0.2',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-10.99.99',
            'dia_ge' => '9.0.0-10.99.99',
            'dia_utilities' => '1.1.0-1.99.99'
        ],
        'conflicts' => [],
        'suggests' => [
            'dia' => '',
        ],
    ],
];
