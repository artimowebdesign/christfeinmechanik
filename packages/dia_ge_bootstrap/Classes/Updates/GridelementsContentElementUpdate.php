<?php

namespace Dia\DiaGeBootstrap\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\RepeatableInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class GridelementsContentElementUpdate
 * @package Dia\DiaGeBootstrap
 */
class GridelementsContentElementUpdate implements UpgradeWizardInterface, RepeatableInterface
{
    /**
     * @var array
     */
    private $gridelementsMapping = [
        1 => 'Column',
        2 => '2Columns',
        3 => '3Columns',
        4 => '4Columns',
        5 => '5Columns',
        6 => '6Columns',
        7 => 'Accordion',
        8 => 'Tabs',
        9 => 'Slider',
        10 => 'Timeline',
        11 => 'ImageGallery',
    ];

    private $flexformReplacement = [
        '/index="(xs|sm|md|lg|xl)"/' => 'index="col_$1"',
        '/index="(xs|sm|md|lg|xl)_(.*)"/' => 'index="$2"',
    ];

    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'DiaDiaGeBootstrapUpdatesGridelementsContentElementUpdate';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return '[DIA GE Bootstrap] Migrate Bootstrap Gridelements';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Update the old Gridlements fields from dia_base';
    }

    /**
     * @param array $record
     * @return array
     */
    protected function formatRecord(array $record): array
    {
        $newRecord = [];
        if (isset($record['pi_flexform'])) {
            $newRecord['pi_flexform'] = $record['pi_flexform'];
            foreach ($this->flexformReplacement as $pattern => $replacement) {
                $newRecord['pi_flexform'] = preg_replace($pattern, $replacement, $newRecord['pi_flexform']);
            }
        }
        if ((int)$record['tx_gridelements_backend_layout'] === 1) {
            $newRecord['pi_flexform'] = '';
        }
        if (isset($record['tx_gridelements_backend_layout'])) {
            $newRecord['tx_gridelements_backend_layout'] = $this->gridelementsMapping[$record['tx_gridelements_backend_layout']] ?? $record['tx_gridelements_backend_layout'];
        }
        return $newRecord;
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     */
    public function executeUpdate(): bool
    {
        $table = 'tt_content';
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $queryBuilder->getRestrictions()->removeAll();
        $statement = $queryBuilder->select('uid', 'tx_gridelements_backend_layout', 'pi_flexform')
            ->from($table)
            ->where(
                $queryBuilder->expr()->eq('CType', $queryBuilder->createNamedParameter('gridelements_pi1', Connection::PARAM_STR)),
                $queryBuilder->expr()->in('tx_gridelements_backend_layout', $queryBuilder->createNamedParameter(
                    array_keys($this->gridelementsMapping),
                    Connection::PARAM_STR_ARRAY
                ))
            )
            ->execute();
        while ($record = $statement->fetch()) {
            $newRecord = $this->formatRecord($record);
            if (!empty($newRecord)) {
                $update = $queryBuilder->update($table)
                    ->where(
                        $queryBuilder->expr()->eq(
                            'uid',
                            $queryBuilder->createNamedParameter($record['uid'], Connection::PARAM_INT)
                        )
                    );
                foreach ($newRecord as $key => $value) {
                    $update->set($key, $value);
                }
                $queryBuilder->execute();
            }
        }

        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll();
        $countQuery = $queryBuilder->count('uid')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq('CType', $queryBuilder->createNamedParameter('gridelements_pi1', Connection::PARAM_STR)),
                $queryBuilder->expr()->in('tx_gridelements_backend_layout', $queryBuilder->createNamedParameter(
                    array_keys($this->gridelementsMapping),
                    Connection::PARAM_STR_ARRAY
                ))
            );
        return (bool)$countQuery->execute()->fetchColumn(0);
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }
}
