<?php
defined('TYPO3_MODE') or die();

// add TsConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:dia_ce_button/Configuration/TsConfig/Page.tsconfig">'
);

// add TypoScript Setup
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:dia_ce_button/Configuration/TypoScript/setup.typoscript">'
);
