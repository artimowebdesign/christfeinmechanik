<?php
defined('TYPO3_MODE') or die();

/**
 * Add Content Element
 */
call_user_func(function ($_EXTKEY, $_TABLE, $CType = 'button', $CTypeIcon = 'content-special-div') {
    /**
     * Language Files
     */
    $languageLocalFilePrefix = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';
    $languageCoreFilePrefix = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:';
    $languageFrontendFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    /**
     * Add content element to selector list
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        $_TABLE,
        'CType',
        [
            $languageLocalFilePrefix . 'tt_content.button.title',
            $CType,
            $CTypeIcon
        ],
        'textmedia',
        'after'
    );

    /**
     * Assign Icon
     */
    $GLOBALS['TCA'][$_TABLE]['ctrl']['typeicon_classes'][$CType] = $CTypeIcon;

    /**
     * Add TCA Columns
     */
    $TCAcolumns = [];
    $TCAcolumns['button_display_block'] = [
        'exclude' => 1,
        'label' => $languageLocalFilePrefix . 'tt_content.button.display_block',
        'config' => [
            'type' => 'check',
            'default' => 0,
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($_TABLE, $TCAcolumns);

    /**
     * Configure palettes
     */
    $GLOBALS['TCA'][$_TABLE]['palettes']['buttonHeaders'] = [
        'showitem' => '
            header;' . $languageLocalFilePrefix . 'tt_content.button.header,
            --linebreak--,
            button_display_block,
            --linebreak--,
            header_layout;' . $languageFrontendFilePrefix . 'header_layout_formlabel,
            header_position,
            --linebreak--,
            header_link;' . $languageFrontendFilePrefix . 'header_link_formlabel
        ',
    ];

    /**
     * Configure element type
     */
    $GLOBALS['TCA'][$_TABLE]['types'][$CType] = [
        'showitem' => '
                --div--;' . $languageCoreFilePrefix . 'general,
                    --palette--;' . $languageFrontendFilePrefix . 'palette.general;general,
                    --palette--;' . $languageFrontendFilePrefix . 'palette.headers;buttonHeaders,
                --div--;' . $languageFrontendFilePrefix . 'tabs.appearance,
                    --palette--;' . $languageFrontendFilePrefix . 'palette.frames;frames,
                    --palette--;' . $languageFrontendFilePrefix . 'palette.appearanceLinks;appearanceLinks,
                --div--;' . $languageCoreFilePrefix . 'language,
                    --palette--;;language,
                --div--;' . $languageCoreFilePrefix . 'access,
                    --palette--;;hidden,
                    --palette--;' . $languageFrontendFilePrefix . 'palette.access;access,
                --div--;' . $languageCoreFilePrefix . 'categories,
                    categories,
                --div--;' . $languageCoreFilePrefix . 'notes,
                    rowDescription,
                --div--;' . $languageCoreFilePrefix . 'extended,
        ',
        'columnsOverrides' => [
            'header_layout' => [
                'config' => [
                    'items' => [
                        0 => [$languageLocalFilePrefix . 'tt_content.button.style.default', 'default'],
                        1 => [$languageLocalFilePrefix . 'tt_content.button.style.primary', 'primary'],
                        2 => [$languageLocalFilePrefix . 'tt_content.button.style.secondary', 'secondary'],
                        3 => [$languageLocalFilePrefix . 'tt_content.button.style.success', 'success'],
                        4 => [$languageLocalFilePrefix . 'tt_content.button.style.danger', 'danger'],
                        5 => [$languageLocalFilePrefix . 'tt_content.button.style.warning', 'warning'],
                        6 => [$languageLocalFilePrefix . 'tt_content.button.style.info', 'info'],
                        7 => [$languageLocalFilePrefix . 'tt_content.button.style.light', 'light'],
                        8 => [$languageLocalFilePrefix . 'tt_content.button.style.dark', 'dark'],
                        9 => [$languageLocalFilePrefix . 'tt_content.button.style.link', 'link'],

                        10 => [$languageLocalFilePrefix . 'tt_content.button.style.outline', '--div--'],
                        11 => [$languageLocalFilePrefix . 'tt_content.button.style.primary', 'outline-primary'],
                        12 => [$languageLocalFilePrefix . 'tt_content.button.style.secondary', 'outline-secondary'],
                        13 => [$languageLocalFilePrefix . 'tt_content.button.style.success', 'outline-success'],
                        14 => [$languageLocalFilePrefix . 'tt_content.button.style.danger', 'outline-danger'],
                        15 => [$languageLocalFilePrefix . 'tt_content.button.style.warning', 'outline-warning'],
                        16 => [$languageLocalFilePrefix . 'tt_content.button.style.info', 'outline-info'],
                        17 => [$languageLocalFilePrefix . 'tt_content.button.style.light', 'outline-light'],
                        18 => [$languageLocalFilePrefix . 'tt_content.button.style.dark', 'outline-dark'],
                    ]
                ]
            ],
        ],
    ];

    /**
     * DIA CE header_style and header_color fields
     */
    if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('dia_ce')) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
            'tt_content',
            'buttonHeaders',
            'header_style',
            'after:header_layout'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
            'tt_content',
            'buttonHeaders',
            'header_color, --linebreak--',
            'before:header_link'
        );
    }
}, 'dia_ce_button', 'tt_content');
