<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'DIA',
    'description' => 'DIA Base Extension',
    'category' => 'distribution',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author_company' => 'DIA - die.interaktiven GmbH & Co. KG',
    'version' => '10.4.15',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.14-10.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
