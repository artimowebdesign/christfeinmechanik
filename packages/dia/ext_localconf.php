<?php
defined('TYPO3_MODE') or die();

// InstallUtility
if (TYPO3_MODE === 'BE') {
    $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
    $signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,
        'afterExtensionInstall',
        \Dia\Utility\InstallUtility::class,
        'afterExtensionInstall'
    );
}

// translate the default tt_content language xlf file
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_ttc.xlf']['dia']
    = 'EXT:dia/Resources/Private/Language/locallang_ttc.xlf';
