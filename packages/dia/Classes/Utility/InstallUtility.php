<?php
namespace Dia\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Configuration\ConfigurationManager;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * InstallUtility
 */
class InstallUtility
{
    /**
     * @var ConfigurationManager
     */
    private $configrationManager;

    /**
     * Emits after the extension has been installed
     *
     * @param string $extensionKey
     * @param object $this
     */
    function afterExtensionInstall($extensionKey, $installUtility = NULL)
    {
        if($extensionKey !== 'dia')
            return;

        // Set backend background image and icon
        $this->setExtensionConfiguration(
            'backend',
            [
                'loginLogo' => 'EXT:dia/Resources/Public/Images/Backend/dia_logo.svg',
                'loginHighlightColor' => '#84bd00',
                'loginBackgroundImage' => 'EXT:dia/Resources/Public/Images/Backend/background.png',
                'loginFootnote' => '',
                'backendLogo' => '',
                'backendFavicon' => '',
            ]
        );
    }

    /**
     * @param $extKey
     * @param array $configuration
     */
    protected function setExtensionConfiguration($extKey, array $configuration)
    {
        $ConfigurationManager = $this->getConfigurationManager();

        $newLocalConfiguration = $ConfigurationManager->getLocalConfigurationValueByPath('EXTENSIONS/' . $extKey);
        ArrayUtility::mergeRecursiveWithOverrule($newLocalConfiguration, $configuration);

        // TYPO3 v9 and above
        $ConfigurationManager->setLocalConfigurationValuesByPathValuePairs([
            'EXTENSIONS/' . $extKey => $newLocalConfiguration
        ]);
    }

    /**
     * @return object|ConfigurationManager
     */
    protected function getConfigurationManager()
    {
        if(!$this->configrationManager) {
            $this->configrationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        }
        return $this->configrationManager;
    }
}
