'use strict';

// turn warnings off
const bluebird = require('bluebird');
bluebird.config({
  warnings: false,
});

const path = require('path');
const twigEngine = require('@frctl/twig');
const fractal = require('@frctl/fractal').create();
const mandelbrot = require('@frctl/mandelbrot')({
  // favicon: '/assets/icons/icon.ico',
  lang: 'en-gb',
  skin: 'lime',
  // styles: ['default', '/assets/styles/theme.css'],
  static: {
    mount: 'fractal'
  }
});

const paths = require('./paths');

// Project config
fractal.set('project.title', 'DIA Frontend');

// Components config
fractal.components.engine(twigEngine);
fractal.components.set('default.preview', '@preview');
fractal.components.set('default.status', null);
fractal.components.set('ext', '.twig');
fractal.components.set('path', paths.components);

// Docs config
// fractal.docs.engine(twigEngine);
// fractal.docs.set('ext', '.md');
// fractal.docs.set('path', paths.docs);

// Web UI config
fractal.web.theme(mandelbrot);
fractal.web.set('server.sync', true);
fractal.web.set('static.path', paths.tmp);
fractal.web.set('builder.dest', paths.build);
fractal.web.set('builder.urls.ext', null);

// Export config
module.exports = fractal;
