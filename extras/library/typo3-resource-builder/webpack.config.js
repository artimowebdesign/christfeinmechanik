
const path = require('path');

// Plugins
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const FractalWebpackPlugin = require('fractal-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const globImporter = require('node-sass-glob-importer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

// Environment
const devMode = process.env.NODE_ENV !== 'production';

// Paths
const paths = require('./paths');

const entries = {};
Object.keys(paths.templates).forEach(key => {
  entries[key] = path.join(paths.templates[key].assets, 'app.js');
});

const plugins = [
  require('autoprefixer'),
  new VueLoaderPlugin(),
  new MiniCssExtractPlugin({
    filename: devMode ? "css/[name].css" : 'css/[name].min.css',
    chunkFilename: devMode ? 'css/[name].css' : 'css/[name].min.css'
  }),
  new CopyWebpackPlugin([
    {
      context: path.resolve(__dirname, '..', 'template', 'assets'),
      from: './**/img/**/*.*',
      to: './',
      ignore: ['fonts/**/*', 'icons/**/*']
    }
  ]),
];

if(devMode) {
  plugins.push(
    new FractalWebpackPlugin({
      mode: 'server',
      sync: true,
      configPath: './fractal.js'
    })
  );
}

module.exports = {
  mode: process.env.NODE_ENV,
  devtool: 'source-map',
  entry: entries,
  output: {
    path: devMode ? paths.tmp : paths.assets,
    filename: devMode ? 'js/[name].js' : 'js/[name].min.js',
    chunkFilename: devMode ? 'js/[name].js' : 'js/[name].min.js'
  },
  watchOptions: {
    poll: 1000,
    ignored: ['node_modules']
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true // set to true if you want JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
      cacheGroups: {
        default: false
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        exclude: [/img/, /styles/],
        use: [
          {
            loader: 'file-loader',
            options: {
              sourceMap: true,
              useRelativePath: false,
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: '../fonts/'
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|tif|svg)$/,
        exclude: [/fonts/],
        use: [
          {
            loader: 'file-loader',
            options: {
              sourceMap: true,
              useRelativePath: false,
              name: '[name]-[sha512:hash:base64:7].[ext]',
              outputPath: 'img/',
              publicPath: '../img/'
            }
          }
        ]
      },
      { test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader', // Run post css actions
            options: {
              sourceMap: true,
              plugins: function () { // post css plugins, can be exported to postcss.config.js
                return [
                  require('autoprefixer'),
                  require('postcss-object-fit-images')
                ];
              }
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                importer: globImporter()
              }
            }
          },
        ]
      }
    ],
  },
  plugins: plugins,
  resolve: {
    symlinks: false
  }
};
