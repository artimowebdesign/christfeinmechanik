# TYPO3 CMS Base Distribution

Get going quickly with TYPO3 CMS.

## Prerequisites

* PHP 7.2 (https://thewebtier.com/php/installing-php-7-2-osx-homebrew/)
* [Composer](https://getcomposer.org/download/)
* set composer global config for composer.diaplatform.de
  `composer config http-basic.composer.diaplatform.de <username> <password>`

## Quickstart

### Create Project
Create an Project in PHPStorm and execute the following commands:

```
git init
git remote add base git@bitbucket.org:dieinteraktiven/typo3-v9-base-distribution.git
git pull base master
git remote remove base
```

or as single line:

```
git init && git remote add base git@bitbucket.org:dieinteraktiven/typo3-v9-base-distribution.git && git pull base master && git remote remove base
```

### Update Project

```
git remote add base git@bitbucket.org:dieinteraktiven/typo3-v9-base-distribution.git && git pull base master && git remote remove base
```

### Local Development Environment with ddev

```
ddev config --docroot=public --create-docroot --project-type=typo3
ddev start
ddev composer install
```

### Setup
If you setup a new project, you can use the typo3cms command directly under ddev:

```
ddev exec bin/typo3cms install:setup
```

### Create DIA Site Extensions
You can generate DIA Site extensions with the following command:

```
ddev exec bin/typo3 dia:site:generate <sitename> <sitename> <sitename>
```

# Extended Documentation

## Create Project

### with Composer (not recommended)

```
composer create-project dia/typo3-v9-base-distribution --repository=https://composer.diaplatform.de project-name ^9
cd project-name
```

### without Composer (not recommended)

```
git init project-name && cd project-name
git pull git@bitbucket.org:dieinteraktiven/typo3-v9-base-distribution.git
```

**Setup:**

To start an interactive installation, you can do so by executing the following
command and then follow the wizard:

```
php bin/typo3cms install:setup
```

**Setup unattended (optional):**

If you're a more advanced user, you might want to leverage the unattended installation.
To do this, you need to execute the following command and substite the arguments
with your own environment configuration.

```
php bin/typo3cms install:setup \
    --non-interactive \
    --database-user-name=typo3 \
    --database-user-password=typo3 \
    --database-host-name=127.0.0.1 \
    --database-port=3306 \
    --database-name=typo3 \
    --use-existing-database \
    --admin-user-name=admin \
    --admin-password=password \
    --site-setup-type=site
```

**Development server:**

While it's advised to use a more sophisticated web server such as
Apache 2 or nginx, you can instantly run the project by using PHPs` built-in
[web server](http://php.net/manual/en/features.commandline.webserver.php).

* `TYPO3_CONTEXT=Development php -S localhost:8000 -t public`
* open your browser at "http://localhost:8000"

Please be aware that the built-in web server is single threaded. Which is ultimately
a performance killer and may result in deadlocks if you execute too many requests at once.


## Local Development Environment with ddev
Install Docker and ddev, according to the following documentation:

[docker Documentation](https://ddev.readthedocs.io/en/stable/users/docker_installation/)  
[ddev Documentation](https://ddev.readthedocs.io/en/stable/)

### Quickstart
Make sure, your Docker is running

```
ddev config --docroot=public --create-docroot --project-type=typo3
ddev start
```

### Setup
If you setup a new project, you can use the typo3cms command directly under ddev:

```
ddev exec bin/typo3cms install:setup
```

## Authenticate on composer.diaplatform.de

You can get the credentials from your Administrator or Lead Developer.

### with local composer

```
composer config http-basic.composer.diaplatform.de <username> <password>
```

### with ddev

```
ddev composer config http-basic.composer.diaplatform.de <username> <password>
ddev composer install
```

## Fractl

To use fractl you need to run ddev and install all pakages with composer.

### Install all dependencies

```
ddev start
ddev composer install
```

### Run Fractl in dev mode with a watcher

```
ddev build -i dev
```

### Build Assets for Prod

```
ddev build -i prod
```

### Update yarn for fractal

```
ddev exec npm install -g yarn
ddev exec yarn -v
```