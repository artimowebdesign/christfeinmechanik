'use strict';

// turn warnings off
const bluebird = require('bluebird');
bluebird.config({
  warnings: false,
});

const path = require('path');
const twigEngine = require('@frctl/twig');
const fractal = require('@frctl/fractal').create();
const mandelbrot = require('@frctl/mandelbrot')({
  // favicon: '/assets/icons/icon.ico',
  lang: 'en-gb',
  skin: 'lime',
  styles: ['default', '/_subtheme/style.css'],
  static: {
    mount: 'fractal'
  }
});

mandelbrot.addLoadPath('./theme-overrides/views');

mandelbrot.addStatic('./theme-overrides', '/_subtheme');

const paths = require('./paths');

// Project config
fractal.set('project.title', 'DIA Frontend');

// Components config
fractal.components.engine(twigEngine);
fractal.components.set('default.preview', '@preview');
fractal.components.set('default.status', null);
fractal.components.set('ext', '.twig');
fractal.components.set('path', paths.components);

fractal.components.set('statuses', {
  prototype: {
    label: "Prototype",
    description: "Do not implement.",
    color: "#dc3545"
  },
  wip: {
    label: "WIP",
    description: "Work in progress. Implement with caution.",
    color: "#ffc107"
  },
  ready: {
    label: "Ready",
    description: "Ready to implement. Default Bootstrap markup is not changed.",
    color: "#28a745"
  },
  changed: {
    label: "Changed",
    description: "Ready to implement. Markup is customized. See notes!",
    color: "#007bff"
  }
});

// Docs config
// fractal.docs.engine(twigEngine);
// fractal.docs.set('ext', '.md');
// fractal.docs.set('path', paths.docs);

// Web UI config
fractal.web.theme(mandelbrot);
fractal.web.set('server.sync', true);
fractal.web.set('static.path', paths.tmp);
fractal.web.set('builder.dest', paths.build);
fractal.web.set('builder.urls.ext', null);

// DDEV URLs generation
if (process.env.IS_DDEV_PROJECT === 'true' && process.env.VIRTUAL_HOST) {
  fractal.web.on('server:created', (server) => {
    server.on('ready', () => {
      server._urls.sync.local = 'http://' + process.env.VIRTUAL_HOST + ':' + server._ports.sync;
      server._urls.sync.external = 'doesn\'t work in ddev mode';
      server._urls.sync.ui = 'http://' + process.env.VIRTUAL_HOST + ':' + (server._ports.sync+2);
    });
  });
}

// Export config
module.exports = fractal;
