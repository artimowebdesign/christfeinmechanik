'use strict';

// turn warnings off
const bluebird = require('bluebird');
bluebird.config({
  warnings: false,
});
const fs = require('fs-extra');
const twigEngine = require('@frctl/twig');
const fractal = require('@frctl/fractal').create();
const mandelbrot = require('@frctl/mandelbrot')({
  // favicon: '/assets/icons/icon.ico',
  lang: 'en-gb',
  skin: 'lime',
  styles: ['default', '/_subtheme/style.css'],
  static: {
    mount: 'fractal'
  }
});

mandelbrot.addLoadPath('./theme-overrides/views');

mandelbrot.addStatic('./theme-overrides', '/_subtheme');

const paths = require('./paths');

// Project config
fractal.set('project.title', 'DIA Frontend');

// Components config
fractal.components.engine(twigEngine);
fractal.components.set('default.preview', '@preview');
fractal.components.set('default.status', null);
fractal.components.set('ext', '.twig');
fractal.components.set('path', paths.components);

fractal.components.set('statuses', {
  prototype: {
    label: "Prototype",
    description: "Do not implement.",
    color: "#dc3545"
  },
  wip: {
    label: "WIP",
    description: "Work in progress. Implement with caution.",
    color: "#ffc107"
  },
  ready: {
    label: "Ready",
    description: "Ready to implement. Default Bootstrap markup is not changed.",
    color: "#28a745"
  },
  changed: {
    label: "Changed",
    description: "Ready to implement. Markup is customized. See notes!",
    color: "#007bff"
  }
});

// Web UI config
fractal.web.theme(mandelbrot);
fractal.web.set('builder.urls.ext', null);
fractal.web.set('builder.dest', '../public/export');
fractal.web.set('static.path', '../public/assets');
fractal.web.set('static.mount', '/');

const builder = fractal.web.builder();

builder.on('progress', (completed, total) => console.log(`Exported ${completed} of ${total} items`, 'info'));
builder.on('error', err => console.error(err.message));

return builder.build().then(() => {
  fs.copyFileSync(__dirname + '/.htaccess.tpl', '../public/export/.htaccess');
  console.log('Fractal build completed!');
});

// Export config
module.exports = fractal;
