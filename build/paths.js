const fs = require('fs');
const del = require('del');
const path = require('path');

// Environment
const devMode = process.env.NODE_ENV !== 'production';

/**
 * @param parentPath
 * @returns {*}
 */
function getDirectories(parentPath) {
  return fs.readdirSync(parentPath).filter(function (childPath) {
    return fs.statSync(path.join(parentPath, childPath)).isDirectory();
  });
}

/**
 * @param directory
 */
function mkdirSyncRecursive(directory) {
  var path = directory.replace(/\/$/, '').split('/');
  for (var i = 1; i <= path.length; i++) {
    var segment = path.slice(0, i).join('/');
    segment.length > 0 && !fs.existsSync(segment) ? fs.mkdirSync(segment) : null ;
  }
};

/**
 * @param paths
 */
function createDirectories(paths) {
  switch(typeof paths) {
    case 'object':
      Object.keys(paths).forEach(key => {
        createDirectories(paths[key]);
      });
      break;
    default:
      if(!fs.existsSync(paths)) {
        console.log('Create folder: ' + paths);
        mkdirSyncRecursive(paths);
      }
  }
}

const publicPath = path.resolve(__dirname, path.join('..', 'public'));
const templatePath = path.resolve(__dirname, path.join('..', 'template'));
const assetsPath = path.join(publicPath, 'assets');

const directories = getDirectories(path.join(templatePath, 'assets'));
const templates = {};

directories.map(function (dir) {
  templates[dir] = {
    assets: path.join(templatePath, 'assets', dir),
    img: path.join(templatePath, 'assets', dir, 'img')
  }
});

const paths = {
  public: publicPath,
  assets: assetsPath,
  templates: templates,
  components: path.join(templatePath, 'components'),
  docs: path.join(templatePath, 'docs'),
  tmp: path.join(templatePath, 'tmp'),
  img: path.join(templatePath, 'tmp', 'img')
};

// Clean
if(devMode) {
  del(path.join(paths.tmp, '*'), {force: true});
} else {
  del(path.join(paths.assets, '*'), {force: true});
}

createDirectories(paths)

module.exports = paths;
